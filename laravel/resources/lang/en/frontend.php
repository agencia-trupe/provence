<?php

return [

    'home' => [
        'titulo_deps' => 'See what our customers say about our service',
        'wpp_frase'   => 'We will respond as soon as possible.',
        'wpp_mensagem'=> 'Be very welcome to Provence. How can we help?',
    ],

    'empresa' => [
        'visao'   => 'Vision',
        'missao'  => 'Mission',
        'valores' => 'Values',
        
        'solicite_proposta' => 'Request your proposal!',
        'solicite_proposta2'=> 'Request your proposal or more information',
        'solicite_proposta3'=> 'Request your quote or more information',
    ],

    'msg_error'    => 'Fill in all the fields correctly',
    'msg_error2'   => 'Fill in all fields correctly and select at least one subject',
    'msg_success'  => 'Message sent successfully!',
    'nome'         => 'Name',
    'nome_empresa' => 'Company Name',
    'cnpj'         => 'CNPJ',
    'nome_contato' => 'Contact name',
    'cargo'        => 'Office',
    'email'        => 'Email',
    'telefone'     => 'Phone',
    'produto'      => 'Product',
    'como_conheceu'=> 'How did you find out about Provence?',
    'situacao_cert'=> 'Certification Status',
    'tipo_cert'    => 'Type of Certification',
    'modelo_cert'  => 'Certification Model',
    'enviar'       => 'Send',

    'contato' => [
        'nome' => 'name',
        'email' => 'email',
        'telefone' => 'phone',
        'mensagem' => 'message',
    ],

    'novidades' => [
        'titulo'     => 'News',
        'nav_titulo' => 'Information and News',
        'nav_desc'   => 'Provence is attentive to everything that happens in the certifications market to share with you important information that can help your business provide more and more confidence with your customers. Check it out!',
    ],

    'perguntas' => [
        'duvidas' => 'If you still have questions - Contact Us',
    ],

    'template' => [
        'appname'    => 'Provence Certifications',
        'frasecook1' => 'We use cookies to personalize content, track advertisements and provide you with a safer browsing experience. By continuing to browse our site, you consent to the use of this information. Read our',
        'frasecook2' => 'and learn more.',
        'aceitecook' => 'ACCEPT AND CLOSE',
    ],

    'header' => [
        'produtos' => 'Products',
        'contato'  => 'Contact',
    ],

    'nav' => [
        'certinmt' => 'Inmetro Product Certifications',
        'certiso'  => 'ISO System Certifications',
        'certprov' => 'Provence Voluntary Certifications',
        'quemsomos'=> 'Who we are',
        'servicos' => 'Our services',
        'infonovs' => 'Information and News',
        'falecon'  => 'Contact us',
    ],

    'footer' => [
        'certsist'  => 'System Certifications',
        'certprov'  => 'Provence certifications',
        'certinmt'  => 'Inmetro certifications',
        'quemsomos' => 'Who we are',
        'servicos'  => 'Our services',
        'suporte'   => 'Support',
        'docsreg'   => 'Regulatory documents',
        'faq'       => 'FAQ',
        'faq2'      => 'Common questions',
        'infonovs'  => 'Information and news',
        'falecon'   => 'Contact us',

        'entrecontato' => 'Contact',

        'whatsapp'  => 'Whatsapp',
        'siga'      => 'Follow',
        'politica'  => 'Privacy Policy',
        'direitos'  => 'All rights reserved',
        'copyright' => 'Provence Certifications All rights reserved',
        'criacao'   => 'Website by',
        'trupe'     => 'Trupe Criative Agency',
    ],
];
