<?php

return [
    'titulo'        => 'titulo_en',
    'titulo_2'      => 'titulo_2_en',
    'sub_titulo'    => 'sub_titulo_en',
    'titulo_conteudo' => 'titulo_conteudo_en',
    'frase'         => 'frase_en',
    'texto'         => 'texto_en',
    'descricao'     => 'descricao_en',
    'nome'          => 'nome_en',
    'visao_descricao'  => 'visao_descricao_en',
    'missao_descricao' => 'missao_descricao_en',
    'valores_descricao'=> 'valores_descricao_en',
    'endereco'      => 'endereco_en',
    'bairro'        => 'bairro_en',
    'cidade'        => 'cidade_en',
];
