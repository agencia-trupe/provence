<?php

return [
    'titulo'        => 'titulo_pt',
    'titulo_2'      => 'titulo_2_pt',
    'sub_titulo'    => 'sub_titulo_pt',
    'titulo_conteudo' => 'titulo_conteudo_pt',
    'frase'         => 'frase_pt',
    'texto'         => 'texto_pt',
    'descricao'     => 'descricao_pt',
    'nome'          => 'nome_pt',
    'visao_descricao'  => 'visao_descricao_pt',
    'missao_descricao' => 'missao_descricao_pt',
    'valores_descricao'=> 'valores_descricao_pt',
    'endereco'      => 'endereco_pt',
    'bairro'        => 'bairro_pt',
    'cidade'        => 'cidade_pt',
];
