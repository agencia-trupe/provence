<?php

return [
    
    'home' => [
        'titulo_deps' => 'Veja o que os nossos clientes dizem sobre o nosso serviço',
        'wpp_frase'   => 'Responderemos o mais breve possível.',
        'wpp_mensagem'=> 'Seja muito bem-vindo(a) à Provence. Como podemos ajudar?',
    ],

    'empresa' => [
        'visao'   => 'Visão',
        'missao'  => 'Missão',
        'valores' => 'Valores',
        
        'solicite_proposta' => 'Solicite sua proposta!',
        'solicite_proposta2'=> 'Solicite sua proposta ou mais informações',
        'solicite_proposta3'=> 'Solicite seu orçamento ou mais informações',
    ],

    'msg_error'    => 'Preencha todos os campos corretamente',
    'msg_error2'   => 'Preencha todos os campos corretamente e selecione ao menos um assunto',
    'msg_success'  => 'Mensagem enviada com sucesso!',
    'nome'         => 'Nome',
    'nome_empresa' => 'Nome da empresa',
    'cnpj'         => 'CNPJ',
    'nome_contato' => 'Nome do contato',
    'cargo'        => 'Cargo',
    'email'        => 'E-mail',
    'telefone'     => 'Telefone',
    'produto'      => 'Produto',
    'como_conheceu'=> 'Como conheceu a Provence?',
    'situacao_cert'=> 'Situação da Certificação',
    'tipo_cert'    => 'Tipo de Certificação',
    'modelo_cert'  => 'Modelo de Certificação',
    'enviar'       => 'Enviar',

    'contato' => [
        'nome' => 'nome',
        'email' => 'e-mail',
        'telefone' => 'telefone',
        'mensagem' => 'mensagem',
    ],

    'novidades' => [
        'titulo'     => 'Novidades',
        'nav_titulo' => 'Informações e novidades',
        'nav_desc'   => 'A Provence está atenta a tudo o que acontece no mercado de certificações para compartilhar com você informações importantes que podem ajudar o seu negócio a oferecer cada vez mais confiança aos seus clientes. Confira!',
    ],

    'perguntas' => [
        'duvidas' => 'Se você ainda tem dúvidas - Fale conosco',
    ],

    'template' => [
        'appname'    => 'Provence Certificações',
        'frasecook1' => 'Usamos cookies para personalizar o conteúdo, acompanhar anúncios e oferecer uma experiência de navegação mais segura a você. Ao continuar navegando em nosso site você concorda com o uso dessas informações. Leia nossa',
        'frasecook2' => 'e saiba mais.',
        'aceitecook' => 'ACEITAR E FECHAR',
    ],

    'header' => [
        'produtos' => 'Produtos',
        'contato'  => 'Contato',
    ],

    'nav' => [
        'certinmt' => 'Certificações de Produtos Inmetro',
        'certiso'  => 'Certificações de Sistemas ISO',
        'certprov' => 'Certificações Voluntárias Provence',
        'quemsomos'=> 'Quem somos',
        'servicos' => 'Nossos Serviços',
        'infonovs' => 'Informações e Novidades',
        'falecon'  => 'Fale Conosco',
    ],

    'footer' => [
        'certsist'  => 'Certificações de Sistemas',
        'certprov'  => 'Certificações Provence',
        'certinmt'  => 'Certificações Inmetro',
        'quemsomos' => 'Quem somos',
        'servicos'  => 'Nossos Serviços',
        'suporte'   => 'Suporte ',
        'docsreg'   => 'Documentos regulamentares',
        'faq'       => 'FAQ',
        'faq2'      => 'Perguntas Frequentes',
        'infonovs'  => 'Informações e novidades',
        'falecon'   => 'Fale Conosco',

        'entrecontato' => 'Entre em contato',
        
        'whatsapp'  => 'Whatsapp',
        'siga'      => 'Siga',
        'politica'  => 'Política de Privacidade',
        'direitos'  => 'Todos os direitos reservados',
        'copyright' => 'Provence Certificações Todos os direitos reservados',
        'criacao'   => 'Criação de sites',
        'trupe'     => 'Trupe Agência Criativa',
    ],
];
