import AjaxSetup from "./AjaxSetup";
import MobileToggle from "./MobileToggle";


AjaxSetup();
MobileToggle();


$(document).ready(function() {
    // AVISO DE COOKIES
    $(".aviso-cookies").hide();

    if (window.location.href == routeHome) {
        $(".aviso-cookies").show();

        $(".aceitar-cookies").click(function() {
            var url = window.location.origin + "/aceite-de-cookies";

            $.ajax({
                type: "POST",
                url: url,
                success: function(data, textStatus, jqXHR) {
                    $(".aviso-cookies").hide();
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    console.log(jqXHR, textStatus, errorThrown);
                    $(".aviso-cookies").hide();
                },
            });
        });
    }
});

// Accordion de FAQ - Perguntas frequentes 2.0
const accordion = document.querySelectorAll('.accordion .contentBx');
accordion.forEach((e)=> {
    e.querySelector('.label').addEventListener('click', ()=> {
        accordion.forEach((removeActive)=> {
            if (removeActive !== e) {
                removeActive.classList.remove('active');
            }
        });
       e.classList.toggle('active');
    });
});


// Proposta Modal
$(window).load(function() {
    if (document.querySelector('#modal-btn-open')) {
        let modalBackground = document.querySelector('.proposta-modal--background');
        let modalForm = document.querySelector('.propsota-modal--form');
        let btnClose  = document.querySelector('#modal-btn-close');

        document.querySelector('#modal-btn-open').addEventListener('click', (e)=> {
            e.preventDefault();
            modalBackground.classList.add('active');
            modalForm.classList.add('active');
        })
        
        btnClose.addEventListener('click', () => {
            modalBackground.classList.remove('active');
            modalForm.classList.remove('active');
        })
    }
})

// Whatsapp floating
    $("#chatWhatsapp").floatingWhatsApp({
        phone: "+55" + whatsappNumero,
        popupMessage: "Seja muito bem-vindo(a) à Provence. Como podemos ajudar?",
        showPopup: true,
        autoOpen: false,
        headerTitle:
            '<div class="img-edc-uni"><img src="' +
            imgMarcaWhatsapp +
            '" alt="" style="width: 90%;"></div>' +
            '<div class="textos"><p class="titulo">Provence</p><p class="frase">Responderemos o mais breve possível.</p></div>',
        headerColor: "#25D366",
        size: "60px",
        position: "right",
        linkButton: true,
    });
