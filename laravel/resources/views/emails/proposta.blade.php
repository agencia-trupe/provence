<!DOCTYPE html>

<html>

<head>

    <title>[CONTATO] {{ config('app.name') }}</title>

    <meta charset="utf-8">

</head>

<body>

    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Nome da Empresa:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $nome_empresa }}</span><br>

@if($nome_contato)

    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Nome do Contato:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $nome_contato }}</span><br>

@endif

    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>E-mail:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $email }}</span><br>

@if($telefone)

    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Telefone:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $telefone }}</span><br>

@endif

@if($cnpj)

    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>CNPJ:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $cnpj }}</span><br>

@endif

@if($cargo)

    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Cargo:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $cargo }}</span><br>

@endif

@if($produto)

    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Produto:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $produto }}</span><br>

@endif

@if($como_conheceu)

    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Como conheceu:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $como_conheceu }}</span><br>

@endif

@if($situacao_cert)

    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Situação Certificado:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $situacao_cert }}</span><br>

@endif

@if($tipo_cert)

    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Tipo de Certificado:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $tipo_cert }}</span><br>

@endif

@if($modelo_cert)

    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Modelo de Certificado:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $modelo_cert }}</span><br>

@endif


    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Mensagem:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $mensagem }}</span>

</body>

</html>

