@extends('frontend.common.template')

@section('content')
    <div class="container home">
        <div class="slideshow-container">

        <!-- Start WOWSlider.com BODY section -->
            <div id="wowslider-container1" style="min-height: 370px;">
                <div class="ws_images">
                <ul>
                    
                    <li><img src="{{ asset('assets/img/banners/img-banner.jpg')}}" alt="responsive slider" title="" id="wows1_0"/></li>                     
                    <li><img src="{{ asset('assets/img/banners/img-banner2.jpg')}}" alt="" title="" id="wows1_1"/></li>
                    <li><img src="{{ asset('assets/img/banners/img-banner3.jpg')}}" alt="" title="" id="wows1_2"/></li>
                </ul>
                </div>
                <div class="ws_shadow"></div>
            </div>	
            <script type="text/javascript" src="{{ asset('assets/js/wowslider/engine1/wowslider.js') }}"></script>
            <script type="text/javascript" src="{{ asset('assets/js/wowslider/engine1/script.js') }}"></script>
            <div class="banner-box">
                <h1>{!! $home->{'banner_'.trans('database.titulo')} !!}</h1>
                {!! $home->{'banner_'.trans('database.descricao')} !!}

                <a href="{!! $home->botao_link !!}"><button class="btn-banner-home">{!! $home->{'botao_'.trans('database.nome')} !!} <img class="icon" src="{{ asset('assets/img/layout/setinha-verde.svg') }}" alt="Saiba mais sobre nossos serviços"></button></a>
            </div>
        </div>
            <br>

        <section class="secao-cards">
            <h1>{!! $home->{'bloco_'.trans('database.titulo')} !!}</h1>
                <article class="article-cards">
                    <div class="external-cards">
                        <a href="{!! $home->bloco_1_link !!}">
                            <figure>
                                <!-- Start WOWSlider.com BODY section -->
                                <div id="wowslider-container5">
                                    <div class="ws_images" style="height: 280px; width: 320px;">
                                        <ul>
                                            @foreach ($items3 as $item)
                                            <li><img src="{{ asset('assets/img/imagensiso/')}}/{!! $item->imagem !!}" id="wows5_0"></li>
                                            @endforeach
                                        </ul>
                                    </div>
                                    <div class="ws_shadow"></div>
                                </div>	
                                <script type="text/javascript" src="{{ asset('assets/js/wowslider/engine5/wowslider.js') }}"></script>
                                <script type="text/javascript" src="{{ asset('assets/js/wowslider/engine5/script.js') }}"></script>
                                <!-- End WOWSlider.com BODY section --> 
                            </figure>
                            <figcaption>
                                <p id="subparagrafo">{!! $home->{'bloco_1_'.trans('database.nome')} !!}</p>
                            </figcaption>
                        </a>                        
                    </div>
                    <div class="external-cards">
                        <a href="{!! $home->bloco_2_link !!}">
                            <figure>
                                <!-- Start WOWSlider.com BODY section -->
                                <div id="wowslider-container3">
                                    <div class="ws_images" style="height: 280px; width: 320px;">
                                        <ul>
                                            @foreach ($items as $item)
                                            <li><img src="{{ asset('assets/img/certificacoes-inmetro/')}}/{!! $item->foto !!}" id="wows3_0"></li>
                                            @endforeach
                                        </ul>
                                    </div>
                                    <div class="ws_shadow"></div>
                                </div>	
                                <script type="text/javascript" src="{{ asset('assets/js/wowslider/engine3/wowslider.js') }}"></script>
                                <script type="text/javascript" src="{{ asset('assets/js/wowslider/engine3/script.js') }}"></script>
                                <!-- End WOWSlider.com BODY section -->
                            </figure>
                            <figcaption>
                                <p>{!! $home->{'bloco_2_'.trans('database.nome')} !!}</p>
                            </figcaption>
                        </a>
                    </div>
                    <div class="external-cards">
                        <a href="{!! $home->bloco_3_link !!}">
                            <figure>
                                <!-- Start WOWSlider.com BODY section -->
                                <div id="wowslider-container4">
                                    <div class="ws_images" style="height: 280px; width: 320px;">
                                        <ul>
                                            @foreach ($items2 as $item2)
                                                <li><img src="{{ asset('assets/img/certificacoes-provence/')}}/{!! $item2->foto !!}" id="wows4_0"></li>
                                            @endforeach
                                        </ul>
                                    </div>
                                    <div class="ws_shadow"></div>
                                </div>	
                                <script type="text/javascript" src="{{ asset('assets/js/wowslider/engine4/wowslider.js') }}"></script>
                                <script type="text/javascript" src="{{ asset('assets/js/wowslider/engine4/script.js') }}"></script>
                                <!-- End WOWSlider.com BODY section -->
                            </figure>
                            <figcaption>
                                <p>{!! $home->{'bloco_3_'.trans('database.nome')} !!}</p>
                            </figcaption>
                        </a>                        
                    </div>
                </article>
        </section>

        
        @if ($novidades && count($novidades) > 0)
        <section class="container chamada-2">
            <div class="container-fluid">
                <article class="row text-center">
                    <h1>{{ trans('frontend.novidades.titulo') }}</h1>
                    <article id="novidades" class="novidades">
                        <div class="row-grid">
                            @foreach ($novidades as $item)
                                <div class="col-4 col-sm-12 col-md-6">
                                    <a href="{{ route('novidades-interna', $item->slug) }}">                            
                                        <figure>
                                            <img src="{{ url('assets/img/informacoes-e-novidades/'.$item->foto) }}" alt="">
                                            <figcaption>
                                                <p>
                                                    {!! $item->{trans('database.titulo')} !!}
                                                </p>
                                            </figcaption>
                                            <img class="seta" src="{{ asset('assets/img/layout/setinha-verde.svg') }}" alt="">
                                        </figure>
                                    </a>
                                </div>
                            @endforeach
                        </div>
                    </article>      
                </article>
            </div>
        </section>
        @endif

        <?php $carrossel = json_decode($depoimentos); ?>
        @if (isset($carrossel) && count($carrossel) > 0)            
        <section class="container chamada-3-carrossel">
            <div class="container-fluid2">
                    <h1>{{ trans('frontend.home.titulo_deps') }}</h1>
                    <div class="owl-carousel owl-theme" style="width:100%">
                        @foreach ($carrossel as $item)
                            <div class="item">
                                <table>
                                    <tr>
                                        <td class="td-img">
                                            <div class="Carousel_image">
                                                <img src="{{ asset('assets/img/depoimentos/') }}/{!! $item->foto !!}" alt="">
                                            </div>
                                        </td>
                                        <td class="td-p" style="width:65%">
                                            {!! $item->{trans('database.descricao')} !!}
                                        </td>
                                    </tr>                              
                                </table>
                            </div>
                        @endforeach
                    </div>
                    
            </div>
        </section>
        @endif
        
@endsection

@section('scripts')
    <script>
        $(document).ready(function(){
        $(".owl-carousel").owlCarousel({
            autoWidth:true,
            dots: false,
            nav: true,
            loop:true,
            responsiveClass:true,
            responsive:{
                0:{
                    items:1,
                    autoWidth:true
                },
                860:{
                    items:1,
                    autoWidth:true
                },
                2600:{
                    items:2,
                    autoWidth:true
                }
            }
        });
        });
    </script>
@endsection