<article class="pdf-download pt-2 pb-3">
    <a href="{{ asset('assets/img/') }}/{!! $path !!}/{!! $registro->documento !!}" download>
        <div class="content2">
            <span class="icon-pdf">
                <img src="{{ asset('assets/img/layout/icone-pdf.svg') }}" alt="Baixar">
            </span>
            <span class="p">{!! $registro->titulo_doc !!}</span> 
                <img src="{{ asset('assets/img/layout/icone-download.svg') }}" alt="">
        </div>
    </a>
</article>