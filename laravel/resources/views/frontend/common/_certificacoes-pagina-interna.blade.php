<section class="container container-article-grid">
    <div class="container-fluid chamada-1-grid p-0">
        <article class="row-grid pagina-interna {{ (isset($mini)) ?'mini':'' }} {{(isset($color) ? $color : '')}}">
            @if (Request::is('certificacoes-provence/*') || Request::is('certificacoes-inmetro/*') || Request::is('certificacoes-de-sistemas/*'))
                <div class="col-12 col-sm-12 col-md-12 text-center p-0">
                    <h3 class="title">Conheça outras certificações provence</h3>
                </div>
            @endif
            @foreach ($registro_items as $item)
                <a href="{{ route($route, $item->slug) }}">
                    <figure>
                        <img src="{{ url('assets/img/'.$path.'/'.$item->foto) }}" alt="">
                        <figcaption>
                            <p>{!! $item->{trans('database.titulo_conteudo')} !!}</p>
                        </figcaption>
                        <div class="filter"></div>
                    </figure>
                </a>
            @endforeach        
        </article>
    </div>
</section>