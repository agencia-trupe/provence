<div class="chamada-orcamento">
    @if (Tools::routeIs('fale-conosco'))
        <h3 class="m-0">{{ trans('frontend.nav.falecon') }}</h3>
    @else        
        <h3>{{ trans('frontend.empresa.solicite_proposta3') }}</h3>
    @endif
    <form action="{{ route('fale-conosco-post') }}" method="post">
        @if($errors->any())
            <div class="flash flash-erro">
                {{ trans('frontend.msg_error2') }}
            </div>
            @endif
            @if(session('enviado'))
            <div class="flash flash-sucesso">{{ trans('frontend.msg_success') }}</div>
            @endif
        <div class="row-grid">
            <div class="col-12 col-md-12 col-sm-12 pr-0">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="text" name="nome" placeholder="{{ trans('frontend.contato.nome') }}">
                <input type="email" name="email" placeholder="{{ trans('frontend.contato.email') }}" >
                <input type="tel" name="telefone" placeholder="{{ trans('frontend.contato.telefone') }}" >
                <textarea name="mensagem" cols="30" rows="@if (Tools::routeIs('fale-conosco')) 5 @else 2 @endif" placeholder="{{ trans('frontend.contato.mensagem') }}"></textarea>
                <button type="submit">{{ trans('frontend.enviar') }} <span class="seta"></span></button>
            </div>
        </div>
    </form>
</div>
