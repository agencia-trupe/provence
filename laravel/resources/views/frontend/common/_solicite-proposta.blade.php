<div class="chamada-proposta {{ Config::get('app.locale') }}">
    <a id="modal-btn-open" class="btn">
        <img src="{{ asset('assets/img/layout/icone-soliciteproposta.svg') }}" alt="">
        <p>{{ trans('frontend.empresa.solicite_proposta') }}</p>
    </a>
</div>
