    <header>
        <div class="idiomas">
            @foreach([
            'pt' => 'PORTUGUÊS',
            'en' => 'ENGLISH',
            ] as $key => $title)
            <a href="{{ route('lang', $key) }}" class="lang">{{ $title }}</a>
            @endforeach
        </div>
        <div class="container">
            <div class="container-fluid header-main">
                <nav id="nav-desktop">
                    @include('frontend.common.nav')
                </nav>
                
                <a href="{{ route('home') }}" class="logo">{{ config('app.name') }}</a>
                <button id="mobile-toggle">
                    <div class="lines"></div>
                </button>
            </div>
        </div>
    </header>

    <nav id="nav-mobile">
        <div class="center">
            @include('frontend.common.nav')
        </div>
    </nav>
