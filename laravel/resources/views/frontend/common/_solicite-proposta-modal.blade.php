<div class="proposta-modal--background {{(session('enviado') || session('error'))?'active':''}}"></div>

<div class="propsota-modal--form {{(session('enviado') || session('error'))?'active':''}}">
    <form action="{{ route('propostas-post') }}" method="post">
        <span id="modal-btn-close" class="proposta-modal--btn-close">x</span>
        <h3>{{ trans('frontend.empresa.solicite_proposta2') }}</h3>
        @if(session('error'))
            <div class="flash flash-erro">
                {{ trans('frontend.msg_error') }}
            </div>
        @endif
        @if(session('enviado'))
            <div class="flash flash-sucesso">{{ trans('frontend.msg_success') }}</div>
        @endif
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <input type="text" name="nome_empresa" placeholder="{{ trans('frontend.nome_empresa') }}" value="{{old('nome_empresa')}}" required>
        <input type="text" name="cnpj" placeholder="{{ trans('frontend.cnpj') }}" value="{{old('cnpj')}}" required>
        <input type="text" name="nome_contato" placeholder="{{ trans('frontend.nome_contato') }}" value="{{old('nome_contato')}}" required>
        <input type="text" name="cargo" placeholder="{{ trans('frontend.cargo') }}" value="{{old('cargo')}}" >
        <input type="text" name="email" placeholder="{{ trans('frontend.email') }}" value="{{old('email')}}" required>
        <input type="text" name="telefone" placeholder="{{ trans('frontend.telefone') }}" value="{{old('telefone')}}">
        <input type="text" name="produto" placeholder="{{ trans('frontend.produto') }}" value="{{old('produto')}}" >
        <select name="como_conheceu" value="{{old('como_conheceu')}}">
            <option disabled selected>{{ trans('frontend.como_conheceu') }}</option>
            @foreach ($opcoes_form_proposta as $item)
                @if ($item->categoria == 'como_conheceu')
                    <option value="{{$item->titulo}}">{{ $item->titulo }}</option>
                @endif
            @endforeach
        </select>
        <select name="situacao_cert" value="{{old('situacao_cert')}}">
            <option disabled selected>{{ trans('frontend.situacao_cert') }}</option>
            @foreach ($opcoes_form_proposta as $item)
                @if ($item->categoria == 'situacao_cert')
                    <option value="{{$item->titulo}}">{{ $item->titulo }}</option>
                @endif
            @endforeach
        </select>
        <select name="tipo_cert" value="{{old('tipo_cert')}}">
            <option disabled selected>{{ trans('frontend.tipo_cert') }}</option>
            @foreach ($opcoes_form_proposta as $item)
                @if ($item->categoria == 'tipo_cert')
                    <option value="{{$item->titulo}}">{{ $item->titulo }}</option>
                @endif
            @endforeach
        </select>
        <select name="modelo_cert" value="{{old('modelo_cert')}}">
            <option disabled selected>{{ trans('frontend.modelo_cert') }}</option>
            @foreach ($opcoes_form_proposta as $item)
                @if ($item->categoria == 'modelo_cert')
                    <option value="{{$item->titulo}}">{{ $item->titulo }}</option>
                @endif
            @endforeach
        </select>
        <textarea name="mensagem" placeholder="Mensagem" required>
            {{old('mensagem')}}
        </textarea>

        <div class="submit">
            <button type="submit">
                <h3>{{ trans('frontend.enviar') }}</h3>
                <img class="seta" src="{{ asset('assets/img/layout/setinha-verde.svg') }}" alt="">
            </button>
        </div>
    </form>
</div>
