<h1 class="titulo">{{ trans('frontend.novidades.nav_titulo') }}</h1>
<p class="suporte-novidades--sub-titulo">
    {{ trans('frontend.novidades.nav_desc') }}
</p>
<aside class="nav-suporte">
    <ul>
        <li class="link @if(Tools::routeIs('documentos-regulamentares')) active @endif"><a href="{{ route('documentos-regulamentares') }}">{{ trans('frontend.footer.docsreg') }}</a></li>
        <li class="link @if(Tools::routeIs('perguntas-frequentes')) active @endif"><a href="{{ route('perguntas-frequentes') }}">{{ trans('frontend.footer.faq') }} &#8226; {{ trans('frontend.footer.faq2') }}</a></li>
        <li class="link @if(Tools::routeIs('informacoes-e-novidades') || Tools::routeIs('novidades-interna')) active @endif"><a href="{{ route('informacoes-e-novidades') }}">{{ trans('frontend.footer.infonovs') }}</a></li>
    </ul>
</aside>