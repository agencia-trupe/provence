<div>
    <a  href="{{ route('certificacoes-inmetro') }}" 
    class="uppercase {{(Tools::routeIs('certificacoes-inmetro')) ? 'active' : ''}}">
        <strong>{{ trans('frontend.nav.certinmt') }}</strong>
    </a>
</div>
<div>
    <a href="{{ route('certificacoes-de-sistemas') }}"
    class="uppercase {{(Tools::routeIs('certificacoes-de-sistemas')) ? 'active' : ''}}">
        <strong>{{ trans('frontend.nav.certiso') }}</strong>
    </a>
</div>
<div>
    <a href="{{ route('certificacoes-provence') }}"
    class="uppercase {{(Tools::routeIs('certificacoes-provence')) ? 'active' : ''}}">
        <strong>{{ trans('frontend.nav.certprov') }}</strong>
    </a>
</div>
<div class="logo-menu">
    <a href="{{ route('home') }}" class="logo">{{ config('app.name') }}</a>
</div>
<div>
    <a href="{{ route('perfil-empresa') }}" @if(Tools::routeIs('perfil-empresa')) class="active" @endif>
        {{ trans('frontend.nav.quemsomos') }}
    </a>
</div>
<div>
    <a href="{{ route('nossos-servicos') }}" @if(Tools::routeIs('nossos-servicos')) class="active" @endif>
        {{ trans('frontend.nav.servicos') }}
    </a>
</div>
<div>
    <a href="{{ route('documentos-regulamentares') }}" @if(Tools::routeIs('suporte-e-novidades/*')) class="active" @endif>
        {{ trans('frontend.nav.infonovs') }}
    </a>
</div>
<div>
    <a href="{{ route('fale-conosco') }}" @if(Tools::routeIs('fale-conosco')) class="active" @endif>
        {{ trans('frontend.nav.falecon') }}
    </a>
</div>
