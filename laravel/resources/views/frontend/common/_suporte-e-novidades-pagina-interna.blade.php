<section class="container container-article-grid">
    <div class="container-fluid p-0">
        <article class="row-grid">            
                @if (Request::is('novidades/*'))
                    <div class="col-12 col-sm-12 col-md-12 text-center p-0">
                        <h3 class="title">Veja mais Informações e novidades</h3>
                    </div>
                @endif                
        </article>

        <article class="row-grid novidades grid-interno">
            @foreach ($items as $i)
                <div class="col-3 col-md-6 col-sm-6">
                    <a href="{{ route('novidades-interna', $i->slug) }}">                            
                        <figure>
                            <img src="{{ asset('assets/img/informacoes-e-novidades/'.$i->foto) }}" alt="{!! $i->titulo !!}">
                            <figcaption>
                                <p>{!! $i->titulo !!}</p>
                            </figcaption>
                            <img class="seta" src="{{ asset('assets/img/layout/setinha-verde.svg') }}" alt="">
                        </figure>
                    </a>
                </div>
            @endforeach            
        </article>
    </div>
</section>