    <footer class="container">
        <div class="container-fluid">
            <div class="row-grid3">
                <div class="col-2 col-sm-6 pt-2">
                    <ul class="menu-footer">
                        <li><a href="{{ route('home') }}"><img width="5" src="{{ asset('assets/img/layout/setinha-branca.svg') }}"> Home</a></li>
                        <li><a href="{{ route('certificacoes-de-sistemas') }}"><img width="5" src="{{ asset('assets/img/layout/setinha-branca.svg') }}"> {{ trans('frontend.footer.certsist') }}</a></li>
                        <li><a href="{{ route('certificacoes-provence') }}"><img width="5" src="{{ asset('assets/img/layout/setinha-branca.svg') }}"> {{ trans('frontend.footer.certprov') }}</a></li>
                        <li><a href="{{ route('certificacoes-inmetro') }}"><img width="5" src="{{ asset('assets/img/layout/setinha-branca.svg') }}"> {{ trans('frontend.footer.certinmt') }}</a></li>
                        <li><a href="#"><img width="5"src="{{ asset('assets/img/layout/setinha-branca.svg') }}"> {{ trans('frontend.footer.quemsomos') }}</a></li>
                    </ul>
                </div>
                <div class="col-sm-6 pt-2">
                    <ul class="menu-footer">
                        <li><img width="5" src="{{ asset('assets/img/layout/setinha-branca.svg') }}"> <a href="{{ route('nossos-servicos') }}">{{ trans('frontend.footer.servicos') }}</a></li>
                        <li><img width="5" src="{{ asset('assets/img/layout/setinha-branca.svg') }}"> <a href="{{ route('documentos-regulamentares') }}">{{ trans('frontend.footer.suporte') }}</a>
                            <ul>
                                <li><a href="{{ route('documentos-regulamentares') }}">{{ trans('frontend.footer.docsreg') }}</a></li>
                                <li><a href="{{ route('perguntas-frequentes') }}">{{ trans('frontend.footer.faq') }} &#8226; {{ trans('frontend.footer.faq2') }}</a></li>
                                <li><a href="{{ route('informacoes-e-novidades') }}">{{ trans('frontend.footer.infonovs') }}</a></li>
                            </ul>
                        </li>
                        <li><a href="{{ route('fale-conosco') }}"> <img width="5" src="{{ asset('assets/img/layout/setinha-branca.svg') }}"> {{ trans('frontend.footer.falecon') }}</a></li>
                    </ul>
                </div>
                <div class="col-3 col-sm-12 text-center">
                    <a href="{{ route('home') }}" class="logo"><img src="{{ asset('assets/img/layout/marca-provence-rodape.svg') }}" alt=""></a>
                    <div class="telefone-footer">
                        <p> <img src="{{ asset('assets/img/layout/icone-telefone.svg') }}" alt="{{ trans('frontend.footer.entrecontato') }}"> {!! $contato->telefone !!}</p>
                        <p><img src="{{ asset('assets/img/layout/icone-whatsapp.svg') }}" alt="{{ trans('frontend.footer.entrecontato') }}">
                            <a href="https://web.whatsapp.com/send?phone={{ $contato->whatsappLink }}" target="_blank">
                                {!! $contato->whatsapp !!}
                            </a>
                        </p>
                    </div>    
                </div>
                <div class="col-2 col-sm-6 pt-2">
                    <div class="redes-sociais-footer">
                        <p>
                            <a href="{!! $contato->link_linkedin !!}"><img src="{{ asset('assets/img/layout/icone-linkedin.svg') }}" alt="{{ trans('frontend.footer.entrecontato') }}"> </a>
                            <a href="{!! $contato->link_youtube !!}"><img src="{{ asset('assets/img/layout/icone-youtube.svg') }}" alt="{{ trans('frontend.footer.entrecontato') }}"> </a>
                            <a href="{!! $contato->link_instagram !!}"><img width="16px" src="{{ asset('assets/img/layout/icone-instagram.svg') }}" alt="{{ trans('frontend.footer.entrecontato') }}"></a>
                            </p> 
                    </div>
                    <p>{!! $contato->{trans('database.endereco')} !!}</p>
                </div>
                <div class="col-2 col-sm-6 pt-2">
                    <ul class="menu-footer">
                        <li><a href="{{ route('politica-de-privacidade') }}"> <img width="5" src="{{ asset('assets/img/layout/setinha-branca.svg') }}"> {{ trans('frontend.footer.politica') }}</a></li>
                    </ul>
                    <p class="small">&copy 2021 {{ trans('frontend.footer.copyright') }}</p>
                    <p class="small">{{ trans('frontend.footer.criacao') }}: <a href="#">{{ trans('frontend.footer.trupe') }}</a></p>
            </div>
        </div>
    </footer>
