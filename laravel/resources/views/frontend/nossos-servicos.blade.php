@extends('frontend.common.template')

@section('content')
<div class="nossos-servicos">
    <section class="container-fluid container-article">
        <article class="row-grid">
            <div class="col-4 col-sm-12 col-md-4 pr-3">
                <h1 class="titulo">{!! $registro->{trans('database.titulo')} !!}</h1>
                <hr class="hr-divisor">
                <h4>{!! $registro->{trans('database.sub_titulo')} !!}</h4>
            </div>
            <div class="col-8 col-sm-12 col-md-8 pl-2">
                {!! $registro->{trans('database.descricao')} !!}
            </div>
        </article>
    </section>

    <section class="container container-article-grid chamada-2">
        <h2>{!! $registro->{trans('database.titulo_2')} !!}</h2>
    </section>
    

    <section class="container chamada-3">
        <div class="container-fluid">
            <article class="row-grid text-center">
                <div class="col-12 col-sm-12 col-md-12">
                        {!! $registro->descricao_3 !!}
                    
                    <h2>{!! $registro->sub_titulo_2 !!}</h2>
                    @if (isset($registro->foto_1))
                        <img src="{{ asset('assets/img/nossos-servicos/'.$registro->foto_1) }}" alt="">
                    @endif
                    @if (isset($registro->foto_2))
                        <img src="{{ asset('assets/img/nossos-servicos/'.$registro->foto_2) }}" alt="">
                    @endif
                </div>
            </article>
        </div>
    </section>
</div>    

@include('frontend.common._solicite-proposta-modal')
<section class="container-fluid container-center">
    @include('frontend.common._solicite-proposta')
</section>
@endsection