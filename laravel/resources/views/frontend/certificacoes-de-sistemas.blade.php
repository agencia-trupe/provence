@extends('frontend.common.template')

@section('content')
    <section class="container-fluid container-article pagine">
        <article class="row-grid content-register">
            <div class="col-4 col-sm-12 col-md-4 pr-3">
                <h1 class="titulo">{!! $registro->{trans('database.titulo')} !!}</h1>
                <hr class="hr-divisor">
                <h4>{!! $registro->{trans('database.sub_titulo')} !!}</h4>
            </div>
            <div class="col-8 col-sm-12 col-md-8 pl-2">
                {!! $registro->{trans('database.descricao')} !!}
                <br>
                @if ($registro->foto)
                    <img class="f-none" src="{{ url('assets/img/certificacoes-de-sistemas/'.$registro->foto) }}" alt="Certificações de sistemas" style="margin-bottom: 60px">
                @endif
                @include('frontend.common._solicite-proposta-modal')
                @include('frontend.common._solicite-proposta')
            </div>
        </article>
    </section>
@endsection