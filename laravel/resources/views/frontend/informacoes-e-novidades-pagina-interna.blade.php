@extends('frontend.common.template')

@section('content')
    <section class="container-fluid container-article">
        <article class="row-grid">
            <div class="col-4 col-sm-12 col-md-4 pr-3">
                @include('frontend.common._suporte-e-novidades-nav')
            </div>
            <div class="col-6 col-sm-12 col-md-8 pl-2 artigo-novidades">
                <div class="img text-center pt-2">
                    <img style="width: 100%" src="{{ url('assets/img/informacoes-e-novidades/'.$item->foto) }}" alt="{!! $item->{trans('database.titulo')} !!}">
                </div>
                <h1>{!! $item->{trans('database.titulo')} !!}</h1>
                {!! $item->{trans('database.descricao')} !!}
                <a href="{{ route('informacoes-e-novidades') }}" class="voltar"><img src="{{ asset('assets/img/layout/setinha-verde.svg') }}" alt="Voltar"> <span><strong>Voltar</strong></span></a>
            </div>
            <span class="col-2 col-sm-0 col-md-0"></span>
        </article>
    </section>

    @include('frontend.common._suporte-e-novidades-pagina-interna')
@endsection