@extends('frontend.common.template')



@section('content')

<div class="suporte-novidades">

    <section class="container-fluid container-article">

        <article class="row-grid">

            <div class="col-4 col-sm-12 col-md-5 pr-3">

                @include('frontend.common._suporte-e-novidades-nav')

            </div>

            <div class="col-8 col-sm-12 col-md-7 pt-2 pl-2">

                <!-- FAQ - Perguntas frequentes 2.0 -->

                <article class="accordion">

                    @foreach ($items as $item)

                        <div class="contentBx">

                            <div class="label">{{ str_replace(array("<p>", "</p>"),'', $item->{trans('database.titulo')}) }}</div>

                            <div class="content">

                                {!! $item->{trans('database.descricao')} !!}

                                <a class="link-fale-conosco" href="{{ route('fale-conosco') }}">

                                    {{ trans('frontend.perguntas.duvidas') }}

                                    <span></span>

                                </a>

                            </div>

                        </div>     

                    @endforeach

                </article>             

            </div>

        </article>

    </section>

</div>

@endsection

