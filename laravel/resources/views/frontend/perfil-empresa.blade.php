@extends('frontend.common.template')

@section('content')

    <div class="perfil-empresa">
        <section class="container-fluid container-article ">
            <article class="row-grid">
                <div class="col-4 col-sm-12 col-md-4 pr-3">
                    <h1 class="titulo">{!! $registro->{trans('database.titulo')} !!}</h1>
                    <hr class="hr-divisor">
                    <h4>{!! $registro->{trans('database.sub_titulo')} !!}</h4>
                </div>
                <div class="col-8 col-sm-12 col-md-8 pl-2">
                    {!! $registro->{trans('database.descricao')} !!}
                </div>
            </article>        
        </section>
    
        <section class="container chamada-2 container-article">
            <article class="container-fluid">
                <div class="row-grid">
                    <div class="col-4 col-sm-12 col-md-4">
                        <div class="left pr-1 pb-2">
                            <span class="h1">{{ trans('frontend.empresa.visao') }}</span>
                            <img src="{{ asset('assets/img/layout/icone-visao.svg') }}" alt="">
                        </div>                        
                    </div>
                    <div class="col-8 col-sm-12 col-md-8 pl-2 perfil-empresa-p">
                        {!! $registro->{trans('database.visao_descricao')} !!}
                    </div>
                </div>
            </article>
            <article class="container-fluid pb-2">
                <div class="row-grid">
                    <div class="col-4 col-sm-12 col-md-4">
                        <div class="left pr-1">
                            <span class="h1">{{ trans('frontend.empresa.missao') }}</span>
                            <img src="{{ asset('assets/img/layout/icone-missao.svg') }}" alt="">
                        </div>                        
                    </div>
                    <div class="col-8 col-sm-12 col-md-8 pl-2 perfil-empresa-p">
                        {!! $registro->{trans('database.missao_descricao')} !!}
                    </div>
                </div>
            </article>
            <article class="container-fluid pb-2">
                <div class="row-grid">
                    <div class="col-4 col-sm-12 col-md-4">
                        <div class="left pr-1">
                            <span class="h1">{{ trans('frontend.empresa.valores') }}</span>
                            <img src="{{ asset('assets/img/layout/icone-valores.svg') }}" alt="">
                        </div>                        
                    </div>
                    <div class="col-8 col-sm-12 col-md-8 pl-2 perfil-empresa-p">
                        {!! $registro->{trans('database.valores_descricao')} !!}
                    </div>
                </div>
            </article>
        </section>
    </div>    

    
    @include('frontend.common._solicite-proposta-modal')
    <section class="container-fluid container-center">
        @include('frontend.common._solicite-proposta')
    </section>
@endsection