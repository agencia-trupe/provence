@extends('frontend.common.template')

@section('content')
    <section class="container-fluid container-article ar pagine">
        <article class="row-grid content-register">
            <div class="col-4 col-sm-12 col-md-4 pr-3">
                <h1 class="titulo">{!! $registro_principal->{trans('database.titulo')} !!}</h1>
                <hr class="hr-divisor">
                <h4>{!! $registro_principal->{trans('database.sub_titulo')} !!}</h4>
                <img class="img-conteudo" src="{{ url('assets/img/certificacoes-inmetro/'.$registro->foto) }}" alt="Certificações Provence">
            </div>
            <div class="col-8 col-sm-12 col-md-8 pl-2">
                <h1 class="titulo-artigo">{!! $registro->{trans('database.titulo_conteudo')} !!}</h1>
                {!! $registro->{trans('database.descricao')} !!}
                <div class="col-8 col-sm-12 col-md-8 pl-2" style="padding: 0;">
                    @if ($registro->documento)                    
                        @include('frontend.common._pdf-download', [
                            'path' => 'certificacoes-inmetro'
                        ])
                    @endif
                </div>
                @include('frontend.common._solicite-proposta-modal')
                @include('frontend.common._solicite-proposta')
            </div>
        </article>
    </section>

    @if ($registro_items)        
        @include('frontend.common._certificacoes-pagina-interna',[
            'path' => 'certificacoes-inmetro',
            'route' => 'inmetro-interna',
            'mini' => true
        ])
    @endif
@endsection