@extends('frontend.common.template')

@section('content')
<div class="suporte-novidades">
    <section class="container-fluid container-article">
        <article class="row-grid">
            <div class="col-4 col-sm-12 col-md-5 pr-3">
                @include('frontend.common._suporte-e-novidades-nav')
            </div>
            <div class="col-8 col-sm-12 col-md-7 pt-2 pl-2">
                <!-- Documentos regulamentares -->
                @if (isset($items))
                    @foreach ($items as $item)
                    <a href="{{ url('assets/img/documentos-regulamentares/'.$item->documento) }}" target="_blank">
                        <article id="pdf-download" class="row-grid pdf-download view-nav-aside">
                            <div class="col-1 col-md-2 p-0">
                                <span class="icon-pdf">
                                    <img src="{{ asset('assets/img/layout/icone-pdf.svg') }}" alt="Baixar">
                                </span>
                            </div>
                            <div class="col-9 col-md-8 text-left pt-g">
                                <span class="p">{!! $item->{trans('database.titulo')} !!}</span> 
                                {!! $item->{trans('database.descricao')} !!}
                            </div>
                            <div class="col-1 col-md-2 text-right">
                                <img class="btn-download" src="{{ asset('assets/img/layout/icone-download.svg') }}" alt="Baixar">
                            </div>
                        </article>  
                    </a>
                    @endforeach
                @endif
                
                      
                       
            </div>
            
        </article>
    </section>
</div>
@endsection
