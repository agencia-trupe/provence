@extends('frontend.common.template')

@section('content')
<div class="fale-conosco">
    <section class="container-fluid container-article fale-conosco--margin">
        <article class="row-grid">
            <div class="col-3 col-sm-12 col-md-5 text-left" style="margin-top: 10px;">

                <div class="contato">
                    <img src="{{ asset('assets/img/layout/icone-whatsapp-vazado.svg') }}" alt="{{ trans('frontend.telefone') }}">
                    <div class="numero">
                        <a href="https://web.whatsapp.com/send?phone={{$contato->whatsappLink}}" target="_blank">
                            {!! $registro->whatsapp !!}
                        </a>
                    </div>
                </div>

                <div class="contato">
                    <img src="{{ asset('assets/img/layout/icone-telefone-verde.svg') }}" alt="{{ trans('frontend.telefone') }}">
                    <div class="numero">
                        {!! $registro->telefone !!}
                    </div>
                </div>

                <div class="contato">
                    <img src="{{ asset('assets/img/layout/icone-localizacao.svg') }}" alt="{{ trans('frontend.telefone') }}">
                    
                    <div class="endereco">
                        <span class="endereco">{!! $registro->{trans('database.endereco')} !!}</span>
                    </div>
                </div>
                
            </div>
            <div class="col-9 col-sm-12 col-md-12 formulario">
                <div class="left" >
                    @include('frontend.common._solicite-orcamento')
                </div>
                <div class="right">
                    @if (isset($registo->google_maps))
                        {!! $registro->google_maps !!}
                    @else            
                        {!! $registro->google_maps !!}
                    @endif
                </div>
            </div>
        </article>
    </section>

    <section class="container google-maps">
       
    </section>
</div>
@endsection
