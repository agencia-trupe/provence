@extends('frontend.common.template')



@section('content')
<!-- 
    <header class="container-center cliente--header">

        <div class="logo-menu">

            <a href="{{ route('home') }}" class="logo">{{ config('app.name') }}</a>

        </div>

    </header> -->



    <section class="container-center cliente--content">

        <h1>Certificados</h1>



        <div class="validacao">

            <div class="imagem">

                <img src="{{ asset('assets/img/validacao/'.$cliente->imagem) }}" alt=""/>

            </div>



            <div class="certificados">

                @if (count($cliente->certificados) > 0)

                    @foreach ($cliente->certificados as $certificado)

                        <a href="{{ url('assets/img/certificados/'.$certificado->documento) }}" target="_blank">

                            {{ $certificado->titulo }}

                            <img src="{{ asset('assets/img/layout/icone-seta-certificacoes.svg') }}" alt="">

                        </a>

                    @endforeach

                @else

                    <span>Não há nenhum certificado!</span>

                @endif

            </div>

        </div>

    </section>

@endsection