@extends('frontend.common.template')

@section('content')
<div class="suporte-novidades">
    <section class="container-fluid container-article">
        <article class="row-grid">
            <div class="col-4 col-sm-12 col-md-5 pr-3">
                @include('frontend.common._suporte-e-novidades-nav')
            </div>
            <div class="col-8 col-sm-12 col-md-7 pt-2 pl-2">
                <!-- Informações e novidades -->
                <article id="novidades" class="novidades">
                    <div class="row-grid2">
                        @foreach ($items as $item)
                            <div class="col-6 col-md-10 col-sm-6">
                                <a href="{{ route('novidades-interna', $item->slug) }}">                            
                                    <figure>
                                        <img src="{{ url('assets/img/informacoes-e-novidades/'.$item->foto) }}" alt="">
                                        <figcaption>
                                            <p>
                                                {!! $item->{trans('database.titulo')} !!}
                                            </p>
                                        </figcaption>
                                        <img class="seta" src="{{ asset('assets/img/layout/setinha-verde.svg') }}" alt="">
                                    </figure>
                                </a>
                            </div>
                        @endforeach
                    </div>
                </article>                
            </div>
            
        </article>
    </section>
</div>
@endsection
