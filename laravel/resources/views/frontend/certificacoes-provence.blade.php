@extends('frontend.common.template')

@section('content')
    <section class="container-fluid container-article">
        <article class="row-grid">
            <div class="col-4 col-sm-12 col-md-4 pr-3">
                <h1 class="titulo">{!! $registro->{trans('database.titulo')} !!}</h1>
                <hr class="hr-divisor">
                <h4>{!! $registro->{trans('database.sub_titulo')} !!}</h4>
            </div>
            <div class="col-8 col-sm-12 col-md-8 pl-2">
                {!! $registro->{trans('database.descricao')} !!}
            </div>
        </article>
    </section>

    @if ($registro_items)        
        @include('frontend.common._certificacoes-pagina-interna',[
            'path' => 'certificacoes-provence',
            'route' => 'provence-interna',
            'color' => 'verde'
        ])
    @endif
    @include('frontend.common._solicite-proposta-modal')
    <section class="container-fluid container-center">
        @include('frontend.common._solicite-proposta')
    </section>
@endsection