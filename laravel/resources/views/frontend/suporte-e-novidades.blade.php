@extends('frontend.common.template')

@section('content')
<div class="suporte-novidades">
    <section class="container-fluid container-article">
        <article class="row-grid">
            <div class="col-4 col-sm-12 col-md-5 pr-3">
                @include('frontend.common._suporte-e-novidades-nav')
            </div>
            <div class="col-8 col-sm-12 col-md-7 pt-2 pl-2">
                <!-- Documentos regulamentares -->
                <article id="pdf-download" class="row-grid pdf-download view-nav-aside">
                    <div class="col-1 col-md-2 p-0">
                        <span class="icon-pdf">
                            <img src="{{ asset('assets/img/layout/icone-pdf.svg') }}" alt="Baixar">
                        </span>
                    </div>
                    <div class="col-9 col-md-8 text-left pt-2">
                        <span class="p">Título do documento para download</span> 
                        <p>Descritivo do arquivo</p>
                    </div>
                    <div class="col-1 col-md-2 text-right">
                        <img class="btn-download" src="{{ asset('assets/img/layout/icone-download.svg') }}" alt="Baixar">
                    </div>
                </article>

                <!-- FAQ - Perguntas frequentes -->
                <article id="faq" class="faq d-none view-nav-aside">
                    <h3>First header</h3>
                    <div>First content panel</div>
                    <h3>Second header</h3>
                    <div>Second content panel</div>
                </article>

                <!-- Informações e novidades -->

                <article id="novidades" class="d-none novidades view-nav-aside">
                    <div class="row-grid">
                        <div class="col-6 col-md-10 col-sm-6">
                            <a href="{{ route('novidades-interna', 'titulo-do-artigo') }}">                            
                                <figure>
                                    <img src="{{ asset('assets/img/layout/cert-provence-barrasapoio.jpg') }}" alt="">
                                    <figcaption>
                                        <p>Título da novidade ou notícia em destaque para a home de novidades</p>
                                    </figcaption>
                                    <img class="seta" src="{{ asset('assets/img/layout/setinha-verde.svg') }}" alt="">
                                </figure>
                            </a>
                        </div>
                    </div>
                </article>                
            </div>
            
        </article>
    </section>
</div>
@endsection
