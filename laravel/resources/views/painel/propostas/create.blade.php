@extends('painel.common.template')

@section('content')

    <legend>      
        <h2>Nova página</h2>
    </legend>

    {!! Form::model($registro,[
        'route'  => ['painel.propostas.store'],
        'method' => 'post',
        'files'  => true])
    !!}

    @include('painel.propostas.form', ['submitText' => 'Cadastrar'])

    {!! Form::close() !!}

@endsection
