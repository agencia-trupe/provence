@extends('painel.common.template')

@section('content')

    <legend>      
        <h2>Editar página</h2>
    </legend>

    {!! Form::model($registro,[
        'route'  => ['painel.propostas.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.propostas.form', ['submitText' => 'Atualizar'])

    {!! Form::close() !!}

@endsection
