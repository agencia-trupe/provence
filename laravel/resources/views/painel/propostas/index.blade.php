@extends('painel.common.template')

@section('content')

    @include('painel.common.flash')

    <legend>
        <h2>
            Propostas
            <a href="{{ route('painel.propostas.create') }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-plus" style="margin-right:10px;"></span>Adicionar Item</a>
        </h2>
    </legend>

    @if(!count($registro))
        <div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>
    @else
        <table class="table table-striped table-bordered table-hover table-info table-sortable" data-table="novidades">
            <thead>
                <tr>
                    <th>Nome da empresa</th>
                    <th>Nome do contato</th>
                    <th>Telefone</th>
                    <th>E-mail</th>
                    <th class="no-filter"><span class="glyphicon glyphicon-cog"></span></th>
                </tr>
            </thead>

            <tbody>
            @foreach ($registro as $item)
                <tr class="tr-row" id="{!! $item->id !!}">
                    <td>    
                        {!! $item->nome_empresa !!}    
                    </td>
                    <td>    
                        {!! $item->nome_contato !!}    
                    </td>
                    <td>    
                        {!! $item->telefone !!}    
                    </td>
                    <td>    
                        {!! $item->email !!}    
                    </td>
                    <td>
                        <div class="">
                            <a href="{{ route('painel.propostas.edit', $item->id ) }}" class="btn btn-primary btn-sm btn-block" style="margin-bottom:10px;">
                                <span class="glyphicon glyphicon-pencil" ></span>Editar
                            </a>
                            
                            {!! Form::open([
                                'route'  => ['painel.propostas.destroy', $item->id],
                                'method' => 'delete'
                            ]) !!}
                            <button type="submit" class="btn btn-danger btn-sm btn-block btn-delete"><span class="glyphicon glyphicon-remove" ></span>Excluir</button>
                            {!! Form::close() !!}
                        </div>

                    </td>
                </tr>
                
            @endforeach
            </tbody>
        </table>
    @endif

@endsection
