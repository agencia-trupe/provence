@include('painel.common.flash')



<div class="form-row">

    <div class="form-group col-md-4 col-sm-12">

        {!! Form::label('nome_empresa', 'Nome da empresa') !!}

        {!! Form::text('nome_empresa', null, ['class' => 'form-control']) !!}

    </div>

    <div class="form-group col-md-4 col-sm-12">

        {!! Form::label('cnpj', 'CNPJ') !!}

        {!! Form::text('cnpj', null, ['class' => 'form-control']) !!}

    </div>

    <div class="form-group col-md-4 col-sm-12">

        {!! Form::label('nome_contato', 'Nome contato') !!}

        {!! Form::text('nome_contato', null, ['class' => 'form-control']) !!}

    </div>

</div>



<div class="form-row">

    <div class="form-group col-md-4 col-sm-12">

        {!! Form::label('cargo', 'Cargo') !!}

        {!! Form::text('cargo', null, ['class' => 'form-control']) !!}

    </div>

    <div class="form-group col-md-4 col-sm-12">

        {!! Form::label('email', 'E-mail') !!}

        {!! Form::text('email', null, ['class' => 'form-control']) !!}

    </div>

    <div class="form-group col-md-4 col-sm-12">

        {!! Form::label('telefone', 'Telefone') !!}

        {!! Form::text('telefone', null, ['class' => 'form-control']) !!}

    </div>

</div>



<div class="form-row">

    <div class="form-group col-md-8 col-sm-12">

        {!! Form::label('produto', 'Produto') !!}

        {!! Form::text('produto', null, ['class' => 'form-control']) !!}

    </div>

    <div class="form-group col-md-4 col-sm-12" style="display: flex; flex-direction: column;">

        {!! Form::label('como_conheceu', 'Como conheceu provence') !!}

        {!! Form::select('como_conheceu', 

            $lista_como_conheceu->pluck('titulo', 'titulo')

        , (isset($registro->como_conheceu))?$registro->como_conheceu:'', 

        ['style' => 'height: 45px;']) !!}

    </div>

</div>



<div class="form-row">

    <div class="form-group col-md-4 col-sm-12" style="display: flex; flex-direction: column;">

        {!! Form::label('situacao_cert', 'Situação certificação') !!}

        {!! Form::select('situacao_cert', 

            $lista_situacao_cert->pluck('titulo', 'titulo')

        , (isset($registro->situacao_cert))?$registro->situacao_cert:'', 

        ['style' => 'height: 45px;']) !!}

    </div>

    <div class="form-group col-md-4 col-sm-12" style="display: flex; flex-direction: column;">

        {!! Form::label('tipo_cert', 'Tipo certificação') !!}

        {!! Form::select('tipo_cert', 

            $lista_tipo_cert->pluck('titulo', 'titulo')

        , (isset($registro->tipo_cert))?$registro->tipo_cert:'', 

        ['style' => 'height: 45px;']) !!}

    </div>

    <div class="form-group col-md-4 col-sm-12" style="display: flex; flex-direction: column;">

        {!! Form::label('modelo_cert', 'Modelo certificação') !!}

        {!! Form::select('modelo_cert', 

            $lista_modelo_cert->pluck('titulo', 'titulo')

        , (isset($registro->modelo_cert))?$registro->modelo_cert:'', 

        ['style' => 'height: 45px;']) !!}

    </div>

</div>



<div class="form-row">

    <div class="form-group col-md-12 col-sm-12">

        {!! Form::label('mensagem', 'Mensagem') !!}

        {!! Form::textarea('mensagem', null, ['class' => 'form-control']) !!}

    </div>

</div>



<div class="form-row">

    {!! Form::submit($submitText, ['class' => 'btn btn-success', 'style' => 'margin-left: 15px']) !!}

    <a href="{{ route('painel.propostas.index') }}" class="btn btn-default btn-voltar">Voltar</a>

</div>