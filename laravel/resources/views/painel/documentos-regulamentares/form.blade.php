@include('painel.common.flash')



    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                {!! Form::label('titulo_pt', 'Título [PT]') !!}
                {!! Form::text('titulo_pt', null, ['class' => 'form-control']) !!}
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group">
                {!! Form::label('titulo_en', 'Título [EN]') !!}
                {!! Form::text('titulo_en', null, ['class' => 'form-control']) !!}
            </div>
        </div>
    </div>
    
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                {!! Form::label('descricao_pt', 'Descrição [PT]') !!}
                {!! Form::textarea('descricao_pt', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group">
                {!! Form::label('descricao_en', 'Descrição [EN]') !!}
                {!! Form::textarea('descricao_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
            </div>
        </div>
    </div>

        

    <div class="form-group">

        {!! Form::label('documento', 'Arquivo') !!}<br>

        @if (!empty($registro->documento))

            <a href="{{ url('assets/img/documentos-regulamentares/'.$registro->documento) }}" target="_blank"> Atual: {!! $registro->documento !!}</a><br>

        @endif


        {!! Form::file('documento', ['class' => 'form-control']) !!}

    </div>





    <div class="form-group ">

        {!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

        <a href="{{ route('painel.documentos-regulamentares.index') }}" class="btn btn-default btn-voltar">Voltar</a>

    </div>

