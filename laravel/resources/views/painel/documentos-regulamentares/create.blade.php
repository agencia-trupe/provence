@extends('painel.common.template')

@section('content')

    <legend>      
        <h2>Novo item</h2>
    </legend>

    {!! Form::model($registro,[
        'route'  => ['painel.documentos-regulamentares.store'],
        'method' => 'post',
        'files'  => true])
    !!}

    @include('painel.documentos-regulamentares.form', ['submitText' => 'Cadastrar'])

    {!! Form::close() !!}

@endsection
