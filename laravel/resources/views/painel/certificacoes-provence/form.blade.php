@include('painel.common.flash')

<div class="row">
	<div class="col-md-6">
		<div class="form-group">
		    {!! Form::label('titulo_pt', 'Título [PT]') !!}
		    {!! Form::text('titulo_pt', null, ['class' => 'form-control']) !!}
		</div>
	</div>

	<div class="col-md-6">
		<div class="form-group">
		    {!! Form::label('titulo_en', 'Título [EN]') !!}
		    {!! Form::text('titulo_en', null, ['class' => 'form-control']) !!}
		</div>
	</div>
</div>

<div class="row">
	<div class="col-md-6">
		<div class="form-group">
		    {!! Form::label('sub_titulo_pt', 'Sub Título [PT]') !!}
		    {!! Form::text('sub_titulo_pt', null, ['class' => 'form-control']) !!}
		</div>
	</div>

	<div class="col-md-6">
		<div class="form-group">
		    {!! Form::label('sub_titulo_en', 'Sub Título [EN]') !!}
		    {!! Form::text('sub_titulo_en', null, ['class' => 'form-control']) !!}
		</div>
	</div>
</div>

<div class="row">
	<div class="col-md-6">
		<div class="form-group">
		    {!! Form::label('descricao_pt', 'Descrição [PT]') !!}
		    {!! Form::textarea('descricao_pt', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
		</div>
	</div>

	<div class="col-md-6">
		<div class="form-group">
		    {!! Form::label('descricao_en', 'Descrição [EN]') !!}
		    {!! Form::textarea('descricao_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
		</div>
	</div>
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}