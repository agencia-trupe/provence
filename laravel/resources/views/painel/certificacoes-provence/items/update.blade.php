@extends('painel.common.template')

@section('content')

    <legend>      
        <h2>Editar página</h2>
    </legend>

    {!! Form::model($registro,[
        'route'  => ['painel.certificacoes-provence-item.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.certificacoes-provence.items.form', ['submitText' => 'Atualizar'])

    {!! Form::close() !!}

@endsection
