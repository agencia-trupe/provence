@extends('painel.common.template')

@section('content')

    <legend>      
        <h2>Nova página</h2>
    </legend>

    {!! Form::model($registro,[
        'route'  => ['painel.certificacoes-provence-item.store'],
        'method' => 'post',
        'files'  => true])
    !!}

    @include('painel.certificacoes-provence.items.form', ['submitText' => 'Cadastrar'])

    {!! Form::close() !!}

@endsection
