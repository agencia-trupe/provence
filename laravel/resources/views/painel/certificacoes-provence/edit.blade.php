@extends('painel.common.template')

@section('content')

    <legend>
        <h2>Certificacões Provence</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.certificacoes-provence.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.certificacoes-provence.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
