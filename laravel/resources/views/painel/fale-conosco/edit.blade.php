@extends('painel.common.template')

@section('content')

    <legend>
        <h2>Fale conosco</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.fale-conosco.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.fale-conosco.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
