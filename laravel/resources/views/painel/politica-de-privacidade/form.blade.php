@include('painel.common.flash')

<div class="row">
	<div class="col-md-6">
		<div class="form-group">
		    {!! Form::label('texto_pt', 'Texto [PT]') !!}
		    {!! Form::textarea('texto_pt', null, ['class' => 'form-control ckeditor', 'data-editor' => 'textEdit']) !!}
		</div>
	</div>

	<div class="col-md-6">
		<div class="form-group">
		    {!! Form::label('texto_en', 'Texto [EN]') !!}
		    {!! Form::textarea('texto_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'textEdit']) !!}
		</div>
	</div>
</div>

<hr>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}