<ul class="nav navbar-nav">

    <li @if(Tools::routeIs('painel.home*')) class="active" @endif>

        <a href="{{ route('painel.home.index') }}">Home</a>

    </li>

    <li class="dropdown @if(Tools::routeIs('painel.certificacoes-inmetro*')) active @endif">

        <a href="#" class="dropdown-toggle" data-toggle="dropdown">

            Cert. Inmetro

            <b class="caret"></b>

        </a>

        <ul class="dropdown-menu">

            <li @if(Tools::routeIs('painel.certificacoes-inmetro.index')) class="active" @endif>

                <a href="{{ route('painel.certificacoes-inmetro.index') }}">Certificacões Inmetro</a>

            </li>

            <li class="divider"></li>

            <li @if(Tools::routeIs('painel.certificacoes-inmetro-item.index')) class="active" @endif>

                <a href="{{ route('painel.certificacoes-inmetro-item.index') }}">Páginas</a>

            </li>

        </ul>

    </li>

    <li class="dropdown @if(Tools::routeIs('painel.certificacoes-de-sistemas*')) active @endif">

        <a href="#" class="dropdown-toggle" data-toggle="dropdown">

            Cert. de Sistemas

            <b class="caret"></b>

        </a>

        <ul class="dropdown-menu">

                <li @if(Tools::routeIs('painel.certificacoes-de-sistemas*')) class="active" @endif>

                    <a href="{{ route('painel.certificacoes-de-sistemas.index') }}">Cert. de Sistemas</a>

                </li>

                <li class="divider"></li>

                <li @if(Tools::routeIs('painel.certificado-de-sistemas-imagens*')) class="active" @endif>

                    <a href="{{ route('painel.certificado-de-sistemas-imagens.index') }}">Imagens</a>

                </li>

            </ul>

        </li>


    <li class="dropdown @if(Tools::routeIs('painel.certificacoes-provence*')) active @endif">

        <a href="#" class="dropdown-toggle" data-toggle="dropdown">

            Cert. Provence

            <b class="caret"></b>

        </a>

        <ul class="dropdown-menu">

            <li @if(Tools::routeIs('painel.certificacoes-provence.index')) class="active" @endif>

                <a href="{{ route('painel.certificacoes-provence.index') }}">Certificacões Provence</a>

            </li>

            <li class="divider"></li>

            <li @if(Tools::routeIs('painel.certificacoes-provence-item.index')) class="active" @endif>

                <a href="{{ route('painel.certificacoes-provence-item.index') }}">Páginas</a>

            </li>

        </ul>

    </li>

    <li @if(Tools::routeIs('painel.perfil-empresa*')) class="active" @endif>

        <a href="{{ route('painel.perfil-empresa.index') }}">Quem Somos</a>

    </li>

    <li @if(Tools::routeIs('painel.nossos-servicos*')) class="active" @endif>

        <a href="{{ route('painel.nossos-servicos.index') }}">Nossos Serviços</a>

    </li>

    <li class="dropdown @if(Tools::routeIs('painel.informacoes-e-novidades*')) active @endif">

        <a href="#" class="dropdown-toggle" data-toggle="dropdown">

            Novidades

            <b class="caret"></b>

        </a>

        <ul class="dropdown-menu">

            <li @if(Tools::routeIs('painel.documentos-regulamentares.index')) class="active" @endif>

                <a href="{{ route('painel.documentos-regulamentares.index') }}">Documentos Relugamentares</a>

            </li>

            <li @if(Tools::routeIs('painel.perguntas-frequentes.index')) class="active" @endif>

                <a href="{{ route('painel.perguntas-frequentes.index') }}">Perguntas Frequentes</a>

            </li>

            <li @if(Tools::routeIs('painel.informacoes-e-novidades.index')) class="active" @endif>

                <a href="{{ route('painel.informacoes-e-novidades.index') }}">Informações e Novidades</a>

            </li>

        </ul>

    </li>

    <li @if(Tools::routeIs('painel.depoimentos*')) class="active" @endif>

        <a href="{{ route('painel.depoimentos.index') }}">Depoimentos</a>

    </li>

    <li @if(Tools::routeIs('painel.fale-conosco*')) class="active" @endif>

        <a href="{{ route('painel.fale-conosco.index') }}">Fale Conosco</a>

    </li>


    <li @if(Tools::routeIs('painel.contato.recebidos*')) class="active" @endif>

        <a href="{{ route('painel.recebidos.index') }}">Contatos Recebidos</a>

    </li>



    <li class="dropdown @if(Tools::routeIs('painel.propostas*')) active @endif">

        <a href="#" class="dropdown-toggle" data-toggle="dropdown">

            Propostas

            <b class="caret"></b>

        </a>

        <ul class="dropdown-menu">

            <li @if(Tools::routeIs('painel.propostas.index')) class="active" @endif>

                <a href="{{ route('painel.propostas.index') }}">Propostas</a>

            </li>

            <li class="divider"></li>

            <li @if(Tools::routeIs('painel.propostas-configuracoes.index')) class="active" @endif>

                <a href="{{ route('painel.propostas-configuracoes.index') }}">Propostas Config</a>

            </li>

        </ul>

    </li>

    <li @if(Tools::routeIs('painel.validacao-certificados*')) class="active" @endif>

        <a href="{{ route('painel.validacao-certificados.index') }}">Validações | Certificados</a>

    </li>

</ul>