@extends('painel.common.template')



@section('content')



    <legend>

        <h2>Quem Somos</h2>

    </legend>



    {!! Form::model($registro, [

        'route'  => ['painel.perfil-empresa.update', $registro->id],

        'method' => 'patch',

        'files'  => true])

    !!}



    @include('painel.perfil-empresa.form', ['submitText' => 'Alterar'])



    {!! Form::close() !!}



@endsection

