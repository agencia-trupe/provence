@include('painel.common.flash')

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('titulo_pt', 'Título [PT]') !!}
            {!! Form::text('titulo_pt', null, ['class' => 'form-control']) !!}
        </div>
    </div>

    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('titulo_en', 'Título [EN]') !!}
            {!! Form::text('titulo_en', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('sub_titulo_pt', 'Sub Título [PT]') !!}
            {!! Form::text('sub_titulo_pt', null, ['class' => 'form-control']) !!}
        </div>
    </div>

    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('sub_titulo_en', 'Sub Título [EN]') !!}
            {!! Form::text('sub_titulo_en', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('descricao_pt', 'Descrição [PT]') !!}
            {!! Form::textarea('descricao_pt', null, ['class' => 'form-control ckeditor', 'data-editor' => 'textBullet']) !!}
        </div>
    </div>

    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('descricao_en', 'Descrição [EN]') !!}
            {!! Form::textarea('descricao_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'textBullet']) !!}
        </div>
    </div>
</div>

<div class="row">
    <div class="form-group col-md-6">
        {!! Form::label('visao_descricao_pt', 'Visão [PT]') !!}
        {!! Form::textarea('visao_descricao_pt', null, ['class' => 'form-control ckeditor', 'data-editor' => 'textBullet']) !!}
    </div>
    <div class="form-group col-md-6">
        {!! Form::label('visao_descricao_en', 'Visão [EN]') !!}
        {!! Form::textarea('visao_descricao_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'textBullet']) !!}
    </div>
</div>
<div class="row">
    <div class="form-group col-md-6">
        {!! Form::label('missao_descricao_pt', 'Missão [PT]') !!}
        {!! Form::textarea('missao_descricao_pt', null, ['class' => 'form-control ckeditor', 'data-editor' => 'textBullet']) !!}
    </div>     
    <div class="form-group col-md-6">
        {!! Form::label('missao_descricao_en', 'Missão [EN]') !!}
        {!! Form::textarea('missao_descricao_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'textBullet']) !!}
    </div>     
</div>
<div class="row">
    <div class="form-group col-md-6">
        {!! Form::label('valores_descricao_pt', 'Valores [PT]') !!}
        {!! Form::textarea('valores_descricao_pt', null, ['class' => 'form-control ckeditor', 'data-editor' => 'textBullet']) !!}
    </div>
    <div class="form-group col-md-6">
        {!! Form::label('valores_descricao_en', 'Valores [EN]') !!}
        {!! Form::textarea('valores_descricao_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'textBullet']) !!}
    </div>
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}