@include('painel.common.flash')

<div class="form-row">
    <div class="form-group col-md-12 col-sm-12">
        {!! Form::label('titulo', 'Título da página') !!}
        {!! Form::text('titulo', null, ['class' => 'form-control']) !!}
    </div>
    <div class="form-group col-md-12 col-sm-12" style="display: flex; flex-direction: column;">
        {!! Form::label('categoria', 'Categoria') !!}
        {!! Form::select('categoria', [
            'como_conheceu' => 'Como conheceu a provence', 
            'situacao_cert' => 'Situação da Certificação',
            'tipo_cert' => 'Tipo de Certificação',
            'modelo_cert' => 'Modelo de Certificação'
        ], (isset($registro->categoria))?$registro->categoria:'', 
        ['style' => 'height: 50px;']); !!}
    </div>
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success', 'style' => 'margin-left: 15px']) !!}