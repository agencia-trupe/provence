@extends('painel.common.template')

@section('content')

    @include('painel.common.flash')

    <legend>
        <h2>
            Configurações Proposta
            <a href="{{ route('painel.propostas-configuracoes.create') }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-plus" style="margin-right:10px;"></span>Adicionar Item</a>
        </h2>
    </legend>

    @if(!count($registro))
        <div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>
    @else
        <!-- Como conheceu a provence ---------->
        <table class="table table-striped table-bordered table-hover table-info table-sortable" data-table="novidades">
            <thead>
                <tr>
                    <th style="width: 80%">Como conheceu a Provence</th>
                    <th class="no-filter"><span class="glyphicon glyphicon-cog"></span></th>
                </tr>
            </thead>

            <tbody>
            @foreach ($registro as $item)
                @if ($item->categoria == "como_conheceu")
                    <tr class="tr-row" id="{!! $item->id !!}">
                        <td>    
                            <li>{!! $item->titulo !!}</li>    
                        </td>
                        <td>
                            <div class="">
                                <a href="{{ route('painel.propostas-configuracoes.edit', $item->id ) }}" class="btn btn-primary btn-sm btn-block" style="margin-bottom:10px;">
                                    <span class="glyphicon glyphicon-pencil" ></span>Editar
                                </a>
                            
                                {!! Form::open([
                                    'route'  => ['painel.propostas-configuracoes.destroy', $item->id],
                                    'method' => 'delete'
                                ]) !!}
                                <button type="submit" class="btn btn-danger btn-sm btn-block btn-delete"><span class="glyphicon glyphicon-remove" ></span>Excluir</button>
                                {!! Form::close() !!}
                            </div>

                        </td>
                    </tr>
                @endif
            @endforeach
            </tbody>
        </table>

        <!-- Situação da Certificação ---------->
        <table class="table table-striped table-bordered table-hover table-info table-sortable" data-table="novidades">
            <thead>
                <tr>
                    <th style="width: 80%">Situação da Certificação</th>
                    <th class="no-filter"><span class="glyphicon glyphicon-cog"></span></th>
                </tr>
            </thead>

            <tbody>
            @foreach ($registro as $item)
                @if ($item->categoria == "situacao_cert")
                    <tr class="tr-row" id="{!! $item->id !!}">
                        <td>    
                            <li>{!! $item->titulo !!}</li>    
                        </td>
                        <td>
                            <div class="">
                                <a href="{{ route('painel.propostas-configuracoes.edit', $item->id ) }}" class="btn btn-primary btn-sm btn-block" style="margin-bottom:10px;">
                                    <span class="glyphicon glyphicon-pencil" ></span>Editar
                                </a>
                            
                                {!! Form::open([
                                    'route'  => ['painel.propostas-configuracoes.destroy', $item->id],
                                    'method' => 'delete'
                                ]) !!}
                                <button type="submit" class="btn btn-danger btn-sm btn-block btn-delete"><span class="glyphicon glyphicon-remove" ></span>Excluir</button>
                                {!! Form::close() !!}
                            </div>

                        </td>
                    </tr>
                @endif
            @endforeach
            </tbody>
        </table>

        <!-- Tipo de Certificação ---------->
        <table class="table table-striped table-bordered table-hover table-info table-sortable" data-table="novidades">
            <thead>
                <tr>
                    <th style="width: 80%">Tipo de Certificação</th>
                    <th class="no-filter"><span class="glyphicon glyphicon-cog"></span></th>
                </tr>
            </thead>

            <tbody>
            @foreach ($registro as $item)
                @if ($item->categoria == "tipo_cert")
                    <tr class="tr-row" id="{!! $item->id !!}">
                        <td>    
                            <li>{!! $item->titulo !!}</li>    
                        </td>
                        <td>
                            <div class="">
                                <a href="{{ route('painel.propostas-configuracoes.edit', $item->id ) }}" class="btn btn-primary btn-sm btn-block" style="margin-bottom:10px;">
                                    <span class="glyphicon glyphicon-pencil" ></span>Editar
                                </a>
                            
                                {!! Form::open([
                                    'route'  => ['painel.propostas-configuracoes.destroy', $item->id],
                                    'method' => 'delete'
                                ]) !!}
                                <button type="submit" class="btn btn-danger btn-sm btn-block btn-delete"><span class="glyphicon glyphicon-remove" ></span>Excluir</button>
                                {!! Form::close() !!}
                            </div>

                        </td>
                    </tr>
                @endif
            @endforeach
            </tbody>
        </table>

        <!-- Modelo de Certificação ---------->
        <table class="table table-striped table-bordered table-hover table-info table-sortable" data-table="novidades">
            <thead>
                <tr>
                    <th style="width: 80%">Modelo de Certificação</th>
                    <th class="no-filter"><span class="glyphicon glyphicon-cog"></span></th>
                </tr>
            </thead>

            <tbody>
            @foreach ($registro as $item)
                @if ($item->categoria == "modelo_cert")
                    <tr class="tr-row" id="{!! $item->id !!}">
                        <td>    
                            <li>{!! $item->titulo !!}</li>    
                        </td>
                        <td>
                            <div class="">
                                <a href="{{ route('painel.propostas-configuracoes.edit', $item->id ) }}" class="btn btn-primary btn-sm btn-block" style="margin-bottom:10px;">
                                    <span class="glyphicon glyphicon-pencil" ></span>Editar
                                </a>
                            
                                {!! Form::open([
                                    'route'  => ['painel.propostas-configuracoes.destroy', $item->id],
                                    'method' => 'delete'
                                ]) !!}
                                <button type="submit" class="btn btn-danger btn-sm btn-block btn-delete"><span class="glyphicon glyphicon-remove" ></span>Excluir</button>
                                {!! Form::close() !!}
                            </div>

                        </td>
                    </tr>
                @endif
            @endforeach
            </tbody>
        </table>
    @endif

@endsection
