@extends('painel.common.template')

@section('content')

    <legend>      
        <h2>Novo Validação</h2>
    </legend>

    {!! Form::model($registro,[
        'route'  => ['painel.validacao-certificados.store'],
        'method' => 'post',
        'files'  => true])
    !!}

    @include('painel.validacao-certificados.form', ['submitText' => 'Cadastrar'])

    {!! Form::close() !!}

@endsection
