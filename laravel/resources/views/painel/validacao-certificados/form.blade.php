@include('painel.common.flash')

    <div class="form-row">
        <div class="form-group col-md-6 col-sm-12" style="padding-left: 0;">
            {!! Form::label('nome_cliente', 'Nome Cliente ou Empresa') !!}
            {!! Form::text('nome_cliente', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group col-md-6 col-sm-12" style="padding-right: 0;">
            {!! Form::label('slug', 'Slug') !!}
            {!! Form::text('slug', null, ['class' => 'form-control']) !!}
        </div>
    </div>

    <div class="form-group">
        {!! Form::label('imagem', 'Foto Upload') !!}<br>
        @if (!empty($registro->imagem))
            <img width="200" src="{{ url('assets/img/validacao/'.$registro->imagem) }}" ><br>
        @endif
        {!! Form::file('imagem', ['class' => 'form-control']) !!}
    </div>
        
    <div class="form-group">
        {!! Form::label('status', 'Modo Ativo') !!}
        {!! Form::checkbox('status', true, ($registro->status)??true) !!}
    </div>

    <div class="form-group ">
        {!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
        <a href="{{ route('painel.validacao-certificados.index') }}" class="btn btn-default btn-voltar">Voltar</a>
    </div>
