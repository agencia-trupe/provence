@extends('painel.common.template')



@section('content')



    @include('painel.common.flash')



    <legend>

        <h2>

            Validações

            <a href="{{ route('painel.validacao-certificados.create') }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-plus" style="margin-right:10px;"></span>Adicionar Item</a>

        </h2>

    </legend>





    @if(!count($items))

    <div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>

    @else

    <table class="table table-striped table-bordered table-hover table-info table-sortable" data-table="novidades">

        <thead>

            <tr>

                <th>Foto</th>

                <th>Detalhes</th>

                <th>URL</th>

                <th class="no-filter"><span class="glyphicon glyphicon-cog"></span></th>

            </tr>

        </thead>



        <tbody>

        @foreach ($items as $item)

            <tr class="tr-row" id="{!! $item->id !!}">

                <td><img width="100" src="{{ asset('assets/img/validacao/')}}/{!! $item->imagem !!}" alt=""></td>
                
                                          

                <td style="width: 30%;">                

                    <ul>

                        <li>Nome: {!! $item->nome_cliente !!}</li>

                        <li>Slug: {!! $item->slug !!}</li>

                        <li>Modo: {{($item->status)?'Ativado':'Desativado'}}</li>

                    </ul>

                </td>

                <td style="width: 35%;">

                    <a href="{{ route('validacao-cliente', ['cliente' => $item->slug]) }}" target="_blank">

                        {{ route('validacao-cliente', ['cliente' => $item->slug]) }}

                    </a>

                </td>

                <td>

                    <div class="">

                        <a href="{{ route('painel.validacao-certificados.edit', $item->id ) }}" class="btn btn-primary btn-sm btn-block" style="margin-bottom:10px;">

                            <span class="glyphicon glyphicon-pencil" ></span> Editar

                        </a>

                        

                        <a href="{{ route('painel.certificados.index', ['id' => $item->id]) }}" class="btn btn-primary btn-sm btn-block" style="margin-bottom:10px;">

                            <span class="glyphicon glyphicon-file" ></span> Certificados

                        </a>

                    

                        {!! Form::open([

                            'route'  => ['painel.validacao-certificados.destroy', $item->id],

                            'method' => 'delete'

                        ]) !!}

                        <button type="submit" class="btn btn-danger btn-sm btn-block btn-delete"><span class="glyphicon glyphicon-remove" ></span> Excluir</button>

                        {!! Form::close() !!}

                    </div>



                </td>

            </tr>

        @endforeach

        </tbody>

    </table>

    @endif



@endsection

