@extends('painel.common.template')

@section('content')

    <legend>      
        <h2>Editar Validação</h2>
    </legend>

    {!! Form::model($registro,[
        'route'  => ['painel.validacao-certificados.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.validacao-certificados.form', ['submitText' => 'Atualizar'])

    {!! Form::close() !!}

@endsection
