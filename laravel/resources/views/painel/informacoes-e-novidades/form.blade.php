@include('painel.common.flash')

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                {!! Form::label('titulo_pt', 'Título [PT]') !!}
                {!! Form::text('titulo_pt', null, ['class' => 'form-control']) !!}
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group">
                {!! Form::label('titulo_en', 'Título [EN]') !!}
                {!! Form::text('titulo_en', null, ['class' => 'form-control']) !!}
            </div>
        </div>
    </div>
    
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                {!! Form::label('descricao_pt', 'Descrição [PT]') !!}
                {!! Form::textarea('descricao_pt', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group">
                {!! Form::label('descricao_en', 'Descrição [EN]') !!}
                {!! Form::textarea('descricao_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
            </div>
        </div>
    </div>

    <div class="form-group">
        {!! Form::label('foto', 'Foto Upload') !!}<br>
        @if (!empty($registro->foto))
            <img width="200" src="{{ url('assets/img/informacoes-e-novidades/'.$registro->foto) }}" ><br>
        @endif
        {!! Form::file('foto', ['class' => 'form-control']) !!}
    </div>
        
    <div class="form-group ">
        {!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
        <a href="{{ route('painel.informacoes-e-novidades.index') }}" class="btn btn-default btn-voltar">Voltar</a>
    </div>
