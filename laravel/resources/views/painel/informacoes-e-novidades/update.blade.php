@extends('painel.common.template')

@section('content')

    <legend>      
        <h2>Editar post</h2>
    </legend>

    {!! Form::model($registro,[
        'route'  => ['painel.informacoes-e-novidades.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.informacoes-e-novidades.form', ['submitText' => 'Atualizar'])

    {!! Form::close() !!}

@endsection
