@extends('painel.common.template')

@section('content')

    <legend>      
        <h2>Novo post</h2>
    </legend>

    {!! Form::model($registro,[
        'route'  => ['painel.informacoes-e-novidades.store'],
        'method' => 'post',
        'files'  => true])
    !!}

    @include('painel.informacoes-e-novidades.form', ['submitText' => 'Cadastrar'])

    {!! Form::close() !!}

@endsection
