@extends('painel.common.template')

@section('content')

    <legend>
        <h2>Certificacões De Sistemas</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.certificacoes-de-sistemas.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.certificacoes-de-sistemas.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
