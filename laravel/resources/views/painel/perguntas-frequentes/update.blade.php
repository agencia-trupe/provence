@extends('painel.common.template')

@section('content')

    <legend>      
        <h2>Editar pergunta</h2>
    </legend>

    {!! Form::model($registro,[
        'route'  => ['painel.perguntas-frequentes.update', $registro->id],
        'method' => 'patch',
        'files'  => false])
    !!}

    @include('painel.perguntas-frequentes.form', ['submitText' => 'Atualizar'])

    {!! Form::close() !!}

@endsection
