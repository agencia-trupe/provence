@extends('painel.common.template')

@section('content')

    <legend>      
        <h2>Novo item</h2>
    </legend>

    {!! Form::model($registro,[
        'route'  => ['painel.perguntas-frequentes.store'],
        'method' => 'post',
        'files'  => false])
    !!}

    @include('painel.perguntas-frequentes.form', ['submitText' => 'Cadastrar'])

    {!! Form::close() !!}

@endsection
