@include('painel.common.flash')



    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                {!! Form::label('titulo_pt', 'Título [PT]') !!}
                {!! Form::textarea('titulo_pt', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group">
                {!! Form::label('titulo_en', 'Título [EN]') !!}
                {!! Form::textarea('titulo_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
            </div>
        </div>
    </div>
    
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                {!! Form::label('descricao_pt', 'Descrição [PT]') !!}
                {!! Form::textarea('descricao_pt', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group">
                {!! Form::label('descricao_en', 'Descrição [EN]') !!}
                {!! Form::textarea('descricao_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
            </div>
        </div>
    </div>

        

    <div class="form-group ">

        {!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

        <a href="{{ route('painel.perguntas-frequentes.index') }}" class="btn btn-default btn-voltar">Voltar</a>

    </div>

