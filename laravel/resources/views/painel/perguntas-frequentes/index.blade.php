@extends('painel.common.template')



@section('content')



    @include('painel.common.flash')



    <legend>

        <h2>

            Perguntas Frequentes

            <a href="{{ route('painel.perguntas-frequentes.create') }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-plus" style="margin-right:10px;"></span>Adicionar Item</a>

        </h2>

    </legend>





    @if(!count($items))

    <div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>

    @else

    <table class="table table-striped table-bordered table-hover table-info table-sortable" data-table="perguntas_frequentes">

        <thead>

            <tr>    

                <th>Ordenar</th>

                <th>Detalhes</th>

                <th class="no-filter"><span class="glyphicon glyphicon-cog"></span></th>

            </tr>

        </thead>



        <tbody>

        @foreach ($items as $item)

        <tr class="tr-row" id="{!! $item->id !!}">

            <td>
                <a href="#" class="btn btn-info btn-sm btn-move">
                    <span class="glyphicon glyphicon-move"></span>
                </a>
            </td>

            <td>

                    <ul>

                        <li>{!! $item->{trans('database.titulo')} !!}</li>

                    </ul>

            </td>

            <td>

                <div class="" style="width:100px; display:flex;">

                    <a href="{{ route('painel.perguntas-frequentes.edit', $item->id ) }}" class="btn btn-primary btn-sm btn-flex btn-rightbox" style="margin-bottom:10px;">

                        <span class="glyphicon glyphicon-pencil" ></span>Editar

                    </a>

                

                    {!! Form::open([

                        'route'  => ['painel.perguntas-frequentes.destroy', $item->id],

                        'method' => 'delete'

                    ]) !!}

                    <button type="submit" class="btn btn-danger btn-sm btn-flex btn-delete btn-leftbox"><span class="glyphicon glyphicon-remove" ></span>Excluir</button>

                    {!! Form::close() !!}

                </div>



            </td>

        </tr>

        @endforeach

        </tbody>

    </table>

    @endif



@endsection

