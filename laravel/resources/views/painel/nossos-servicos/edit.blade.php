@extends('painel.common.template')

@section('content')

    <legend>
        <h2>Nossos Serviços</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.nossos-servicos.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.nossos-servicos.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
