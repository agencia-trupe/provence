@include('painel.common.flash')

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('titulo_pt', 'Título [PT]') !!}
            {!! Form::text('titulo_pt', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('titulo_en', 'Título [EN]') !!}
            {!! Form::text('titulo_en', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('sub_titulo_pt', 'Sub Título [PT]') !!}
            {!! Form::text('sub_titulo_pt', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('sub_titulo_en', 'Sub Título [EN]') !!}
            {!! Form::text('sub_titulo_en', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('descricao_pt', 'Descrição [PT]') !!}
            {!! Form::textarea('descricao_pt', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
        </div>
    </div>
    
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('descricao_en', 'Descrição [EN]') !!}
            {!! Form::textarea('descricao_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
        </div>
    </div>
</div>
<hr>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('titulo_2_pt', 'Título bloco 2 [PT]') !!}
            {!! Form::text('titulo_2_pt', null, ['class' => 'form-control']) !!}
        </div>
    </div>
        
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('titulo_2_en', 'Título bloco 2 [EN]') !!}
            {!! Form::text('titulo_2_en', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

<div class="form-group">
        {!! Form::label('foto_1', 'Foto 1 Upload') !!}<br>
        @if (!empty($registro->foto_1))
            <img width="200" src="{{ url('assets/img/nossos-servicos/'.$registro->foto_1) }}" ><br>
        @endif
        {!! Form::file('foto_1', ['class' => 'form-control']) !!}
</div>

<div class="form-group">
        {!! Form::label('foto_2', 'Foto 2 Upload') !!}<br>
        @if (!empty($registro->foto_2))
            <img width="200" src="{{ url('assets/img/nossos-servicos/'.$registro->foto_2) }}" ><br>
        @endif
        {!! Form::file('foto_2', ['class' => 'form-control']) !!}
</div>

@if (!empty($registro->foto_1))
    <div class="form-row">
        <div class="form-group col-md-12"> 
            {!! Form::label('remove_foto_1', 'Limpar imagem 1') !!}<br>
            {!! Form::radio('remove_foto_1', 'true', false); !!}
        </div>
    </div>
@endif

@if (!empty($registro->foto_2))
    <div class="form-row">
        <div class="form-group col-md-12"> 
            {!! Form::label('remove_foto_2', 'Limpar imagem 2') !!}<br>
            {!! Form::radio('remove_foto_2', 'true', false); !!}
        </div>
    </div>
@endif

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}