@extends('painel.common.template')



@section('content')



    @include('painel.common.flash')



    <legend>

        <h2>

            Páginas internas

            <a href="{{ route('painel.certificacoes-inmetro-item.create') }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-plus" style="margin-right:10px;"></span>Adicionar Item</a>

        </h2>

    </legend>





    @if(!count($items))


    <div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>

    @else

    <table class="table table-striped table-bordered table-hover table-info table-sortable" data-table="certificacoes_inmetro_item">

        <thead>

            <tr>

                <th>Ordenar</th>

                <th>Foto</th>

                <th>Detalhes</th>

                <th class="no-filter"><span class="glyphicon glyphicon-cog"></span></th>

            </tr>

        </thead>



        <tbody>

        @foreach ($items as $item)

        <tr class="tr-row" id="{!! $item->id !!}">
            
            <td>
                <a href="#" class="btn btn-info btn-sm btn-move">
                    <span class="glyphicon glyphicon-move"></span>
                </a>
            </td>

            <td><img width="100" src="{{ asset('assets/img/certificacoes-inmetro/')}}/{!! $item->foto !!}" alt=""></td>

            <td>

                    <ul>

                        <!-- <li>Título: {!! $item->titulo !!}</li> -->

                        <li>{!! $item->{trans('database.titulo_conteudo')} !!}</li>

                        <!-- <li>Sub título: {!! $item->sub_titulo !!}</li> -->

                    </ul>

            </td>

            <td style="min-width: 120px;">

                <div class="btn-group btn-group-sm" style="width: 0;">

                    <a href="{{ route('painel.certificacoes-inmetro-item.edit', $item->id ) }}" class="btn btn-primary btn-sm pull-left" style="margin-bottom:10px;">

                        <span class="glyphicon glyphicon-pencil" style="margin-right:10px;" ></span>Editar

                    </a>

                

                    {!! Form::open([

                        'route'  => ['painel.certificacoes-inmetro-item.destroy', $item->id],

                        'method' => 'delete'

                    ]) !!}

                    <button type="submit" class="btn btn-danger btn-sm btn-delete" style="border-bottom-left-radius: 0px; border-top-left-radius: 0px;"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>

                    {!! Form::close() !!}

                </div>



            </td>

        </tr>

        @endforeach

        </tbody>

    </table>

    @endif



@endsection

