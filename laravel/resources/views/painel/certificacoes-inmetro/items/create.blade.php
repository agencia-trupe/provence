@extends('painel.common.template')

@section('content')

    <legend>      
        <h2>Nova página</h2>
    </legend>

    {!! Form::model($registro,[
        'route'  => ['painel.certificacoes-inmetro-item.store'],
        'method' => 'post',
        'files'  => true])
    !!}

    @include('painel.certificacoes-inmetro.items.form', ['submitText' => 'Cadastrar'])

    {!! Form::close() !!}

@endsection
