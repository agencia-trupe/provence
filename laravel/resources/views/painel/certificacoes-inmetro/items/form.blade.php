@include('painel.common.flash')



        <!-- <div class="form-row">

            <div class="form-group col-md-6 col-sm-12">

                {!! Form::label('titulo', 'Título da página') !!}

                {!! Form::text('titulo', null, ['class' => 'form-control']) !!}

            </div>

            <div class="form-group col-md-6 col-sm-12">

                {!! Form::label('sub_titulo', 'Sub título') !!}

                {!! Form::text('sub_titulo', null, ['class' => 'form-control']) !!}

            </div>

        </div> -->



        <div class="form-row">

            <div class="col-md-6">
                <div class="form-group">
                    {!! Form::label('titulo_conteudo_pt', 'Título conteúdo [PT]') !!}
                    {!! Form::text('titulo_conteudo_pt', null, ['class' => 'form-control']) !!}
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    {!! Form::label('titulo_conteudo_en', 'Título conteúdo [EN]') !!}
                    {!! Form::text('titulo_conteudo_en', null, ['class' => 'form-control']) !!}
                </div>
            </div>

            <!-- <div class="form-group col-md-2 col-6" style="padding-top: 25px">

                {!! Form::label('formulario', 'Formulário de contato:') !!}

            </div>     

            <div class="form-group col-md-1 col-6" style="padding-top: 25px">

                {!! Form::label('formulario', 'Desativado') !!}

                {!! Form::radio('formulario', 'false', false); !!}

            </div>

            <div class="form-group col-md-1 col-6" style="padding-top: 25px">

                {!! Form::label('formulario', 'Ativado') !!}<br>

                {!! Form::radio('formulario', 'true', false); !!}

            </div> -->

        </div>

        

        <div class="form-row">

            <div class="col-md-6">
                <div class="form-group">
                    {!! Form::label('descricao_pt', 'Descrição [PT]') !!}
                    {!! Form::textarea('descricao_pt', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    {!! Form::label('descricao_en', 'Descrição [EN]') !!}
                    {!! Form::textarea('descricao_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
                </div>
            </div>

        </div>

        <div class="form-row ">

            <div class="form-group col-md-6"> 

                {!! Form::label('documento', 'Documento para download') !!}

                {!! Form::file('documento', ['class' => 'form-control']) !!}

                @if (!empty($registro->documento))

                    <a href="{{ url('assets/img/certificacoes-inmetro/'.$registro->documento) }}" target="_blank">Ver documento</a><br>

                @endif

            </div>

            <div class="form-group col-md-6 col-sm-12">

                {!! Form::label('titulo_doc', 'Título do Documento') !!}

                {!! Form::text('titulo_doc', null, ['class' => 'form-control']) !!}

            </div>

        </div>  

        

        <div class="form-row">

            <div class="form-group col-md-12">

                {!! Form::label('foto', 'Foto Upload') !!}<br>

                @if (!empty($registro->foto))

                    <img width="200" src="{{ url('assets/img/certificacoes-inmetro/'.$registro->foto) }}" ><br>

                @endif

                {!! Form::file('foto', ['class' => 'form-control']) !!}

            </div>

        </div>



        @if (!empty($registro->documento))

            <div class="form-row">

                <div class="form-group col-md-12"> 

                    {!! Form::label('remove_documento', 'Limpar documento e imagem') !!}<br>

                    {!! Form::radio('remove_documento', 'true', false); !!}

                </div>

            </div>

        @endif





<div class="form-row">

    <div class="form-group col-md-12 col-sm-12">

        {!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

        <a href="{{ route('painel.certificacoes-inmetro-item.index') }}" class="btn btn-default btn-voltar">Voltar</a>

    </div>

</div>

