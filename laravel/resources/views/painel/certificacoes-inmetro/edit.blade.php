@extends('painel.common.template')

@section('content')

    <legend>
        <h2>Certificacões Inmetro</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.certificacoes-inmetro.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.certificacoes-inmetro.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
