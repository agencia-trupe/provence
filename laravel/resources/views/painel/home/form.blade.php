@include('painel.common.flash')

<div class="well form-group">
    {!! Form::label('banner_foto', 'Imagem do banner') !!}
@if($submitText == 'Alterar' && $registro->banner_foto)
    <img src="{{ url('assets/img/home/'.$registro->banner_foto) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
@endif
    {!! Form::file('banner_foto', null, ['class' => 'form-control']) !!}
</div>

<div class="row">
    <div class="form-group col-md-6">
        {!! Form::label('banner_titulo_pt', 'Banner Título [PT]') !!}
        {!! Form::text('banner_titulo_pt', null, ['class' => 'form-control']) !!}
    </div>
    <div class="form-group col-md-6">
        {!! Form::label('banner_titulo_en', 'Banner Título [EN]') !!}
        {!! Form::text('banner_titulo_en', null, ['class' => 'form-control']) !!}
    </div>
</div>

<div class="row">
    <div class="form-group col-md-6">
        {!! Form::label('banner_descricao_pt', 'Banner descrição [PT]') !!}
        {!! Form::textarea('banner_descricao_pt', null, ['class' => 'form-control ckeditor', 'data-editor' => 'clean']) !!}
    </div>
    <div class="form-group col-md-6">
        {!! Form::label('banner_descricao_en', 'Banner descrição [EN]') !!}
        {!! Form::textarea('banner_descricao_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'clean']) !!}
    </div>
</div>

<div class="row">
    <div class="col-md-4">
        {!! Form::label('botao_nome_pt', 'Botão nome [PT]') !!}
        {!! Form::text('botao_nome_pt', null, ['class' => 'form-control']) !!}
    </div>
    <div class="col-md-4">
        {!! Form::label('botao_nome_en', 'Botão nome [EN]') !!}
        {!! Form::text('botao_nome_en', null, ['class' => 'form-control']) !!}
    </div>
    <div class="col-md-4">
        {!! Form::label('botao_link', 'Botão link') !!}
        {!! Form::text('botao_link', null, ['class' => 'form-control']) !!}
    </div>
</div>    

<hr>

<div class="form-group">
    <div class="row mb-4">
        <div class="col-md-6">
            {!! Form::label('bloco_titulo_pt', 'Título dos blocos [PT]') !!}
            {!! Form::text('bloco_titulo_pt', null, ['class' => 'form-control']) !!}
        </div>
        <div class="col-md-6">
            {!! Form::label('bloco_titulo_en', 'Título dos blocos [EN]') !!}
            {!! Form::text('bloco_titulo_en', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>
<div class="form-group">
    <div class="row">
        <div class="col-md-3">
            {!! Form::label('bloco_1_nome_pt', 'Bloco 1 (nome) [PT]') !!}
            {!! Form::text('bloco_1_nome_pt', null, ['class' => 'form-control']) !!}
        </div>
        <div class="col-md-3">
            {!! Form::label('bloco_1_nome_en', 'Bloco 1 (nome) [EN]') !!}
            {!! Form::text('bloco_1_nome_en', null, ['class' => 'form-control']) !!}
        </div>
        <div class="col-md-3">
            {!! Form::label('bloco_1_link', 'Bloco 1 (link)') !!}
            {!! Form::text('bloco_1_link',  null, ['class' => 'form-control']) !!}
        </div>
        <div class="col-md-3">
            {!! Form::label('grid_foto', 'Bloco 1 (imagem)') !!}
            <img src="{{ url('assets/img/home/'.$registro->bloco_1_foto ?? null) }}" style="display:block; margin-bottom: 10px; max-width: 30%;">
            {!! Form::file('bloco_1_foto', null, ['class' => 'form-control']) !!}
        </div>
    </div> 
</div> 
<div class="form-group">
    <div class="row">
        <div class="col-md-3">
            {!! Form::label('bloco_2_nome_pt', 'Bloco 2 (nome) [PT]') !!}
            {!! Form::text('bloco_2_nome_pt', null, ['class' => 'form-control']) !!}
        </div>
        <div class="col-md-3">
            {!! Form::label('bloco_2_nome_en', 'Bloco 2 (nome) [EN]') !!}
            {!! Form::text('bloco_2_nome_en', null, ['class' => 'form-control']) !!}
        </div>
        <div class="col-md-3">
            {!! Form::label('bloco_2_link', 'Bloco 2 (link)') !!}
            {!! Form::text('bloco_2_link', null, ['class' => 'form-control']) !!}
        </div>
        <div class="col-md-3">
            {!! Form::label('grid_foto', 'Bloco 2 (imagem)') !!}
            <img src="{{ url('assets/img/home/'.$registro->bloco_2_foto ?? null) }}" style="display:block; margin-bottom: 10px; max-width: 30%;">
            {!! Form::file('bloco_2_foto', null, ['class' => 'form-control']) !!}
        </div>
    </div> 
</div> 
<div class="form-group">
    <div class="row">
        <div class="col-md-3">            
            {!! Form::label('bloco_3_nome_pt', 'Bloco 3 (nome) [PT]') !!}
            {!! Form::text('bloco_3_nome_pt', null, ['class' => 'form-control']) !!}
        </div>
        <div class="col-md-3">            
            {!! Form::label('bloco_3_nome_en', 'Bloco 3 (nome) [EN]') !!}
            {!! Form::text('bloco_3_nome_en', null, ['class' => 'form-control']) !!}
        </div>
        <div class="col-md-3">
            {!! Form::label('bloco_3_link', 'Bloco 3 (link)') !!}
            {!! Form::text('bloco_3_link', null, ['class' => 'form-control']) !!}
        </div>
        <div class="col-md-3">
            {!! Form::label('bloco_3_foto', 'Bloco 3 (imagem)') !!}
            <img src="{{ url('assets/img/home/'.$registro->bloco_3_foto ?? null) }}" style="display:block; margin-bottom: 10px; max-width: 30%;">
            {!! Form::file('bloco_3_foto', null, ['class' => 'form-control']) !!}
        </div>
    </div> 
</div> 

<hr>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
