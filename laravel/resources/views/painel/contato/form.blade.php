@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('creci', 'CRECI') !!}
    {!! Form::text('creci', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('email', 'E-mail') !!}
    {!! Form::email('email', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('email_sac', 'E-mail SAC') !!}
    {!! Form::email('email_sac', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('email_imobiliaria', 'E-mail Imobiliária') !!}
    {!! Form::email('email_imobiliaria', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('telefone', 'Telefone') !!}
    {!! Form::text('telefone', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('whatsapp', 'WhatsApp') !!}
    {!! Form::text('whatsapp', null, ['class' => 'form-control']) !!}
</div>

<hr>

<div class="form-group">
    {!! Form::label('endereco', 'Endereço') !!}
    {!! Form::textarea('endereco', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
</div>

<div class="form-group">
    {!! Form::label('google_maps', 'Código GoogleMaps') !!}
    {!! Form::text('google_maps', null, ['class' => 'form-control']) !!}
</div>

<hr>

<div class="form-group">
    {!! Form::label('youtube', 'YouTube') !!}
    {!! Form::text('youtube', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('facebook', 'Facebook') !!}
    {!! Form::text('facebook', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('instagram', 'Instagram') !!}
    {!! Form::text('instagram', null, ['class' => 'form-control']) !!}
</div>

<hr>

<div class="form-group">
    {!! Form::label('como_contratar', 'Como contratar') !!}
    {!! Form::textarea('como_contratar', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
</div>

<div class="row">
    <div class="col-md-6">
        <div class="well form-group">
            {!! Form::label('como_contratar_imagem_1', 'Como contratar - Imagem 1') !!}
        @if($submitText == 'Alterar' && $contato->como_contratar_imagem_1)
            <img src="{{ url('assets/img/contato/'.$contato->como_contratar_imagem_1) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
        @endif
            {!! Form::file('como_contratar_imagem_1', ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="well form-group">
            {!! Form::label('como_contratar_imagem_2', 'Como contratar - Imagem 2') !!}
        @if($submitText == 'Alterar' && $contato->como_contratar_imagem_2)
            <img src="{{ url('assets/img/contato/'.$contato->como_contratar_imagem_2) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
        @endif
            {!! Form::file('como_contratar_imagem_2', ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
