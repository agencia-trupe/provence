@extends('painel.common.template')

@section('content')

    <legend>      
        <h2>Editar Certificado cliente: {!! $cliente->nome_cliente !!}</h2>
    </legend>

    {!! Form::model($registro,[
        'route'  => ['painel.certificados.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.certificados.form', ['submitText' => 'Atualizar'])

    {!! Form::close() !!}

@endsection
