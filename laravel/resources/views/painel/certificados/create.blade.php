@extends('painel.common.template')

@section('content')

    <legend>      
        <h2>Novo Certificado cliente: {!! $cliente->nome_cliente !!}</h2>
    </legend>

    {!! Form::model($registro,[
        'route'  => ['painel.certificados.store'],
        'method' => 'post',
        'files'  => true])
    !!}

    @include('painel.certificados.form', ['submitText' => 'Cadastrar'])

    {!! Form::close() !!}

@endsection
