@include('painel.common.flash')

    <input type="hidden" name="id_validacao" value="{{ $cliente->id }}" />

    <div class="form-row">
        <div class="form-group col-md-6 col-sm-12" style="padding-left: 0;">
            {!! Form::label('titulo', 'Título do certificado') !!}
            {!! Form::text('titulo', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group col-md-6 col-sm-12" style="padding-left: 0;">
            {!! Form::label('validade', 'Data de validade') !!}
            {!! Form::date('validade', null, ['class' => 'form-control']) !!}
        </div>
    </div>

    <div class="form-row">
        <div class="form-group col-md-12" style="padding-left: 0;"> 
            {!! Form::label('documento', 'Documento para download') !!}
            {!! Form::file('documento', ['class' => 'form-control']) !!}
            @if (!empty($registro->documento))
                <a href="{{ url('assets/img/certificados/'.$registro->documento) }}" target="_blank">Ver documento</a><br>
            @endif
        </div>
    </div>
        
    <div class="form-group">
        {!! Form::label('status', 'Modo Ativo') !!}
        {!! Form::checkbox('status', true, ($registro->status)??true) !!}
    </div>

    <div class="form-group ">
        {!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
        <a href="{{ route('painel.certificados.index', ['id' => $cliente->id]) }}" class="btn btn-default btn-voltar">Voltar</a>
    </div>
