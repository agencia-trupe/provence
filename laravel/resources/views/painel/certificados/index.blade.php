@extends('painel.common.template')

@section('content')

    @include('painel.common.flash')

    <legend>
        <h2>
            Certificados cliente: {!! $registro->nome_cliente !!}
            <a href="{{ route('painel.certificados.create', ['id' => $registro->id]) }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-plus" style="margin-right:10px;"></span>Adicionar Item</a>
        </h2>
    </legend>


    @if(!count($items))
    <div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>
    @else
    <table class="table table-striped table-bordered table-hover table-info table-sortable" data-table="novidades">
        <thead>
            <tr>
                <th>Título</th>
                <th>Data de validade</th>
                <th>Modo Ativo</th>
                <th class="no-filter"><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($items as $item)
            <tr class="tr-row" id="{!! $item->id !!}">
                <td style="width: 30%;">                
                    {!! $item->titulo !!}
                </td>
                <td style="width: 35%;">
                    {!! date('d / m / Y', strtotime($item->validade)) !!} 
                    {!! ($item->validade < date('Y-m-d')) ? '- Vencido' : '' ; !!}
                </td>
                <td >
                    {!! ($item->status)? 'Ativado': 'Desativado' ;  !!}
                </td>
                <td>
                    <div class="">
                        <a href="{{ route('painel.certificados.edit', $item->id ) }}" class="btn btn-primary btn-sm btn-block" style="margin-bottom:10px;">
                            <span class="glyphicon glyphicon-pencil" ></span> Editar
                        </a>
                    
                        {!! Form::open([
                            'route'  => ['painel.certificados.destroy', $item->id],
                            'method' => 'delete'
                        ]) !!}
                        <button type="submit" class="btn btn-danger btn-sm btn-block btn-delete"><span class="glyphicon glyphicon-remove" ></span> Excluir</button>
                        {!! Form::close() !!}
                    </div>

                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    @endif

@endsection
