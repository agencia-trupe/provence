@extends('painel.common.template')

@section('content')

    <legend>      
        <h2>Editar depoimento</h2>
    </legend>

    {!! Form::model($registro,[
        'route'  => ['painel.depoimentos.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.depoimentos.form', ['submitText' => 'Atualizar'])

    {!! Form::close() !!}

@endsection
