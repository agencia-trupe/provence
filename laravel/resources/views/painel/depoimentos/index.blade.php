@extends('painel.common.template')



@section('content')



    @include('painel.common.flash')



    <legend>

        <h2>

            Depoimentos

            <a href="{{ route('painel.depoimentos.create') }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-plus" style="margin-right:10px;"></span>Adicionar Item</a>

        </h2>

    </legend>





    @if(!count($items))

    <div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>

    @else

    <table class="table table-striped table-bordered table-hover table-info table-sortable" data-table="novidades">

        <thead>

            <tr>

                <th>Foto</th>

                <th>Detalhes</th>

                <th class="no-filter"><span class="glyphicon glyphicon-cog"></span></th>

            </tr>

        </thead>



        <tbody>

        @foreach ($items as $item)

        <tr class="tr-row" id="{!! $item->id !!}">

            <td><img width="100" src="{{ asset('assets/img/depoimentos/')}}/{!! $item->foto !!}" style="justify-content:center;" alt=""></td>

            <td style="width: 60%;">                

                <ul>

                    <li>Título: {!! $item->{trans('database.descricao')} !!}</li>

                </ul>

            </td>

            <td>

                <div class="">

                    <a href="{{ route('painel.depoimentos.edit', $item->id ) }}" class="btn btn-primary btn-sm btn-block" style="margin-bottom:10px;">

                        <span class="glyphicon glyphicon-pencil" ></span>Editar

                    </a>

                

                    {!! Form::open([

                        'route'  => ['painel.depoimentos.destroy', $item->id],

                        'method' => 'delete'

                    ]) !!}

                    <button type="submit" class="btn btn-danger btn-sm btn-block btn-delete"><span class="glyphicon glyphicon-remove" ></span>Excluir</button>

                    {!! Form::close() !!}

                </div>



            </td>

        </tr>

        @endforeach

        </tbody>

    </table>

    @endif



@endsection

