@extends('painel.common.template')

@section('content')

    <legend>      
        <h2>Novo depoimento</h2>
    </legend>

    {!! Form::model($registro,[
        'route'  => ['painel.depoimentos.store'],
        'method' => 'post',
        'files'  => true])
    !!}

    @include('painel.depoimentos.form', ['submitText' => 'Cadastrar'])

    {!! Form::close() !!}

@endsection
