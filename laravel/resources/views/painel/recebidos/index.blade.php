@extends('painel.common.template')

@section('content')

@include('painel.common.flash')

<legend class="mb-4">
    <h2 class="m-0">CONTATOS RECEBIDOS</h2>
</legend>

@if(!count($contatos))
<div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>
@else
<div class="table-responsive">
    <table class="table table-striped table-bordered table-hover table-sortable" data-table="contatos_recebido">
        <thead>
            <tr>
                <th scope="col">Data</th>
                <th scope="col">Nome</th>
                <th scope="col">E-mail</th>
                <th class="no-filter" scope="col"><i class="bi bi-gear-fill me-2"></th>
            </tr>
        </thead>

        <tbody>
            @foreach ($contatos as $contato)
            <tr id="{{ $contato->id }}" style="color: black;">
                <td data-order="{{ $contato->created_at_order }}">{{ $contato->created_at }}</td>
                <td>{{ $contato->nome }}</td>
                <td class="d-flex flex-row align-items-center">
                    {{ $contato->email }}
                </td>
                <td class="crud-actions">
                    {!! Form::open([
                    'route' => ['painel.recebidos.destroy', $contato->id],
                    'method' => 'delete'
                    ]) !!}

                    <div class="btn-group btn-group-sm" role="group">
                        <a href="{{ route('painel.recebidos.show', $contato->id ) }}" class="btn btn-primary btn-sm">
                            <i class="bi bi-chat-text-fill me-2"></i>Ler mensagem
                        </a>

                        <button type="submit" class="btn btn-danger btn-sm btn-delete"><i class="bi bi-trash-fill me-2"></i>Excluir</button>

                    </div>

                    {!! Form::close() !!}
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>

@endif

@endsection