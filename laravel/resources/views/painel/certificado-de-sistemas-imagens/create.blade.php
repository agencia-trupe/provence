@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Imagens Iso /</small> Adicionar Imagem</h2>
    </legend>

    {!! Form::open(['route' => 'painel.certificado-de-sistemas-imagens.store', 'files' => true]) !!}

        @include('painel.certificado-de-sistemas-imagens.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
