@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Certificado de Sistemas - Imagens /</small> Editar Imagens</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.certificado-de-sistemas-imagens.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.certificado-de-sistemas-imagens.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
