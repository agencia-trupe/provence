<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePropostasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('propostas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nome_empresa');
            $table->string('cnpj');
            $table->string('nome_contato');
            $table->string('cargo');
            $table->string('email');
            $table->string('telefone');
            $table->string('produto');
            $table->string('como_conheceu');
            $table->string('situacao_cert');
            $table->string('tipo_cert');
            $table->string('modelo_cert');
            $table->text('mensagem');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('propostas');
    }
}
