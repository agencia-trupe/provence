<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInformacoesENovidadesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('informacoes_e_novidades', function(Blueprint $table){
            $table->increments('id');
            $table->integer('ordem');
            $table->text('titulo');
            $table->string('slug');
            $table->text('descricao');
            $table->text('foto');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('informacoes_e_novidades');
    }
}
