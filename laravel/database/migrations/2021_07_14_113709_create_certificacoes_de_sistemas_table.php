<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCertificacoesDeSistemasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('certificacoes_de_sistemas', function (Blueprint $table){
            $table->increments('id');
            $table->string('titulo');
            $table->string('sub_titulo');
            $table->string('documento');
            $table->text('descricao');
            $table->string('foto');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('certificacoes_de_sistemas');
    }
}
