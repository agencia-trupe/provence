<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNossosServicosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nossos_servicos', function (Blueprint $table){
            $table->increments('id');
            $table->string('titulo');
            $table->string('sub_titulo');
            $table->text('descricao');
            $table->string('titulo_2');
            $table->string('foto_1');
            $table->string('foto_2');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('nossos_servicos');
    }
}
