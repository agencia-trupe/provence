<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCertificacoesInmetroTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('certificacoes_inmetro', function(Blueprint $table){
            $table->increments('id');
            $table->string('titulo');
            $table->string('sub_titulo');
            $table->string('documento');
            $table->text('descricao');
            $table->timestamps();
        });

        Schema::create('certificacoes_inmetro_item', function(Blueprint $table){
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->string('titulo');
            $table->string('sub_titulo');
            $table->string('titulo_conteudo');
            $table->string('slug');
            $table->text('descricao');
            $table->string('documento');
            $table->string('titulo_doc');
            $table->string('foto');
            $table->string('formulario');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('certificacoes_inmetro');
        Schema::drop('certificacoes_inmetro_item');
    }
}
