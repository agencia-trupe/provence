<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHomeTable extends Migration
{
    public function up()
    {
        Schema::create('home', function (Blueprint $table) {
            $table->increments('id');
            $table->text('banner_titulo');
            $table->text('banner_descricao');
            $table->text('botao_nome');
            $table->text('botao_link');
            $table->text('banner_foto');
            $table->text('bloco_titulo');
            $table->text('bloco_1_nome');
            $table->text('bloco_1_link');
            $table->text('bloco_1_foto');
            $table->text('bloco_2_nome');
            $table->text('bloco_2_link');
            $table->text('bloco_2_foto');
            $table->text('bloco_3_nome');
            $table->text('bloco_3_link');
            $table->text('bloco_3_foto');
            $table->text('titulo_2');
            $table->text('link_youtube');
            $table->text('carrossel_foto');
            $table->text('carrossel_descricao');
            $table->text('carrossel');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('home');
    }
}
