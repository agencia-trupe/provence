<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCertificadosDeValidacaoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('certificados_de_validacao', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_validacao');
            $table->string('titulo');
            $table->string('documento');
            $table->date('validade');
            $table->boolean('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('certificacoes_de_validacoes');
    }
}
