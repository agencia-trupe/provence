<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePerfilEmpresaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('perfil_empresa', function (Blueprint $table){
            $table->increments('id');
            $table->string('titulo');
            $table->string('sub_titulo');
            $table->string('visao_descricao');
            $table->string('missao_descricao');
            $table->string('valores_descricao');
            $table->text('descricao');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('perfil_empresa');
    }
}
