<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImagensIsoTable extends Migration
{
    public function up()
    {
        Schema::create('imagensiso', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->string('imagem');
            $table->string('link');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('imagensiso');
    }
}
