<?php

use Illuminate\Database\Seeder;

class CertificacoesInmetro extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('certificacoes_inmetro')->insert([
            'titulo' => '',
            'sub_titulo' => '',
            'descricao' => '',
        ]);
    }
}
