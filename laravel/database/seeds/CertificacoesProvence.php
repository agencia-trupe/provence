<?php

use Illuminate\Database\Seeder;

class CertificacoesProvence extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('certificacoes_provence')->insert([
            'titulo' => '',
            'sub_titulo' => '',
            'descricao' => '',
        ]);
    }
}
