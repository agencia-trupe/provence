<?php

use Illuminate\Database\Seeder;

class CertificacoesDeSistemas extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('certificacoes_de_sistemas')->insert([
            'titulo' => '',
            'sub_titulo' => '',
            'descricao' => '',
        ]);
    }
}
