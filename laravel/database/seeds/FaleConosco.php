<?php

use Illuminate\Database\Seeder;

class FaleConosco extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('fale_conosco')->insert([
            'telefone' => '',
            'whatsapp' => '',
            'endereco' => '',
        ]);
    }
}
