<?php

use Illuminate\Database\Seeder;

class NossosServicos extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {   
        DB::table('nossos_servicos')->insert([
            'titulo' => 'Nossos serviços',
            'sub_titulo' => 'Por que nos contratar?',
            'descricao' => 'Descrição de teste',
            'titulo_2' => 'Conheça o passo a passo dos processos de certificação',
            'foto_1' => '',
            'foto_2' => ''
        ]);
    }
}
