<?php

use Illuminate\Database\Seeder;

class PerfilEmpresa extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('perfil_empresa')->insert([
            'titulo' => '',
            'sub_titulo' => '',
            'descricao' => '',
        ]);
    }
}
