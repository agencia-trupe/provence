<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class CertificacoesInmetroItem extends Model
{

    protected $table = 'certificacoes_inmetro_item';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public static function uploadImagem1()
    {
        return CropImage::make('foto', [
            [
                'width'   => 320,
                'height'  => 280,
                'path'    => 'assets/img/certificacoes-inmetro/'
            ],
        ]);
    }

}


