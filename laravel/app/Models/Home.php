<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Home extends Model
{
    protected $table = 'home';

    protected $guarded = ['id'];

    public static function uploadImagem1()
    {
        return CropImage::make('bloco_1_foto', [
            [
                'width'   => 320,
                'height'  => 280,
                'path'    => 'assets/img/home/'
            ],
        ]);
    }

    public static function uploadImagem2()
    {
        return CropImage::make('bloco_2_foto', [
            [
                'width'   => 320,
                'height'  => 280,
                'path'    => 'assets/img/home/'
            ],
        ]);
    }

    public static function uploadImagem3()
    {
        return CropImage::make('bloco_3_foto', [
            [
                'width'   => 320,
                'height'  => 280,
                'path'    => 'assets/img/home/'
            ],
        ]);
    }
}
