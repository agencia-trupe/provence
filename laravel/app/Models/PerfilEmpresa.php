<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PerfilEmpresa extends Model
{
    protected $table = 'perfil_empresa';

    protected $guarded = ['id'];
}
