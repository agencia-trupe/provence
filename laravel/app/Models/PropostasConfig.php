<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PropostasConfig extends Model
{
    protected $table = 'propostas_config';

    protected $guarded = ['id'];
}
