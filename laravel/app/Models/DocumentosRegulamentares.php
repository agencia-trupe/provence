<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DocumentosRegulamentares extends Model
{
    protected $table = 'documentos_regulamentares';

    protected $guarded = ['id'];
}
