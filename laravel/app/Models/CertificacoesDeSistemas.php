<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CertificacoesDeSistemas extends Model
{
    protected $table = 'certificacoes_de_sistemas';

    protected $guarded = ['id'];
}
