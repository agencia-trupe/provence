<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Validacao extends Model
{
    protected $table = 'validacoes';

    protected $guarded = ['id'];

    public function certificados()
    {
        return $this->hasMany(CertificadosDeValidacao::class, 'id_validacao', 'id');
    }
}
