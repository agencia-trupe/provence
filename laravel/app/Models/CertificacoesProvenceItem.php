<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class CertificacoesProvenceItem extends Model
{
    protected $table = 'certificacoes_provence_item';

    protected $guarded = ['id'];

    public static function uploadImagem1()
    {
        return CropImage::make('foto', [
            [
                'width'   => 320,
                'height'  => 280,
                'path'    => 'assets/img/certificacoes-provence/'
            ],
        ]);
    }
}
