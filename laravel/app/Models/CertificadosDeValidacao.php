<?php

namespace App\Models;

use App\Models\Validacao;
use Illuminate\Database\Eloquent\Model;

class CertificadosDeValidacao extends Model
{
    protected $table = 'certificados_de_validacao';

    protected $guarded = ['id'];

    public function cliente()
    {
        return $this->belongsTo(Validacao::class, 'id_validacao', 'id');
    }
}
