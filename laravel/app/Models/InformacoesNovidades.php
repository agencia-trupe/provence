<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class InformacoesNovidades extends Model
{
    protected $table = 'informacoes_e_novidades';
    protected $guarded = ['id'];

    public static function uploadFoto()
    {
        return CropImage::make('foto', [
            [
                'width'   => 380,
                'height'  => 200,
                'path'    => 'assets/img/informacoes-e-novidades/'
            ],
        ]);
    }
}
