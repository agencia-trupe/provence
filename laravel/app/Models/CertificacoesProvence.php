<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CertificacoesProvence extends Model
{
    protected $table = 'certificacoes_provence';

    protected $guarded = ['id'];
}
