<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NossosServicos extends Model
{
    protected $table = 'nossos_servicos';
    protected $guarded = ['id'];
}
