<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CertificacoesInmetro extends Model
{
    protected $table = 'certificacoes_inmetro';

    protected $guarded = ['id'];
}
