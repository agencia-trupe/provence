<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Schema\Builder;
use App\Models\FaleConosco;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('*', function($view) {
            $contato = \App\Models\FaleConosco::first();
            $formatWhatsapp = explode(" ", $contato->whatsapp);
            $contato->whatsappLink = implode('', $formatWhatsapp);

            $view->with('config', \App\Models\Configuracoes::first());
            $view->with('contato', $contato);
        });

        view()->composer('frontend.*', function ($view) {
            $view->with('contatos', FaleConosco::first());
        });

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
