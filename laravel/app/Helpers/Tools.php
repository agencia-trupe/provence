<?php

namespace App\Helpers;

use Illuminate\Support\Facades\File;

class Tools
{
    public static function routeIs($routeNames)
    {
        foreach ((array) $routeNames as $routeName) {
            if (str_is($routeName, \Route::currentRouteName())) {
                return true;
            }
        }

        return false;
    }

    public static function arrayToString(array $array){
        $nome = "";
        foreach($array as $item){
            $nome .= $item .',';
        }
        $nome = substr($nome, 0,-1);
                
        return $nome;
    }

    public static function fileUpload($file, $path){
        $file_name = "";
        $file->move(public_path('assets/img/'.$path), time() . '-' . $file->getClientOriginalName());
        $file_name = time() .'-'. $file->getClientOriginalName();

        return $file_name;
    }

    public static function fileUploadMutiple($files, $path){
        $file_name = "";
        foreach($files as $file){
            $file->move(public_path('assets/img/'.$path), time() . '-' . $file->getClientOriginalName());
            $file_name .= time() .'-'. $file->getClientOriginalName().',';
        }
        $file_name = substr($file_name, 0, -1);

        return $file_name;
    }

    public static function removeFileUpload($file, $path) {
        $file_name = "";
        $file_path = public_path('assets/img/'.$path.$file);
        if(File::exists($file_path)) {
            File::delete($file_path);
        }
    }

}
