<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class NossosServicosRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'titulo_pt' => 'required',
            'sub_titulo_pt' => 'required',
            'descricao_pt' => 'required',
            'titulo_2_pt' => 'required'
        ];
    }
}
