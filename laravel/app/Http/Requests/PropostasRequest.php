<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class PropostasRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nome_empresa' => 'required',
            'cnpj' => 'required',
            'nome_contato' => 'required',
            'cargo' => 'required',
            'email' => 'required|email',
            'telefone' => 'required',
            'produto' => '',
            'como_conheceu' => '',
            'situacao_cert' => '',
            'tipo_cert' => '',
            'modelo_cert' => '',
        ];
    }
}
