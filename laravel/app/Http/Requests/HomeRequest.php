<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class HomeRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'banner_titulo_pt' => 'required',
            'banner_descricao_pt' => 'required',
            'botao_nome_pt' => 'required',
            'botao_link' => 'required',
            'bloco_1_nome_pt' => 'required',
            'bloco_2_nome_pt' => 'required',
            'bloco_3_nome_pt' => 'required',
            'bloco_1_link' => 'required',
            'bloco_2_link' => 'required',
            'bloco_3_link' => 'required'
        ];
    }
}
