<?php

Route::group(['middleware' => ['web']], function () {
    Route::get('/', 'HomeController@index')->name('home');
    Route::get('certificacoes-provence', 'CertificacoesController@provence')->name('certificacoes-provence');
    Route::get('certificacoes-provence/{internal?}', 'CertificacoesController@provencePaginaInterna')->name('provence-interna');
    Route::get('certificacoes-inmetro', 'CertificacoesController@inmetro')->name('certificacoes-inmetro');
    Route::get('certificacoes-inmetro/{internal?}', 'CertificacoesController@inmetroPaginaInterna')->name('inmetro-interna');
    Route::get('certificacoes-de-sistemas', 'CertificacoesController@deSistemas')->name('certificacoes-de-sistemas');
    Route::get('perfil-empresa', 'HomeController@perfilEmpresa')->name('perfil-empresa');
    Route::get('nossos-servicos', 'HomeController@nossosServicos')->name('nossos-servicos');
    Route::get('suporte-e-novidades/documentos-regulamentares', 'NovidadesController@documentosRegulamentares')->name('documentos-regulamentares');
    Route::get('suporte-e-novidades/perguntas-frequentes', 'NovidadesController@perguntasFrequentes')->name('perguntas-frequentes');
    Route::get('suporte-e-novidades/novidades', 'NovidadesController@novidades')->name('informacoes-e-novidades');
    Route::get('novidades/{internal?}', 'NovidadesController@novidadesPaginaInterna')->name('novidades-interna');
    Route::get('fale-conosco', 'FaleConoscoController@index')->name('fale-conosco');
    Route::post('fale-conosco', 'FaleConoscoController@post')->name('fale-conosco-post');
    Route::get('politica-de-privacidade', 'HomeController@politicaDePrivacidade')->name('politica-de-privacidade');
    Route::post('aceite-de-cookies', 'HomeController@postCookies')->name('aceite-de-cookies.post');
    Route::post('propostas-post', 'PropostasController@post')->name('propostas-post');
    
    // Idiomas
    Route::get('lang/{idioma?}', function ($idioma = 'pt') {
        if (in_array($idioma, ['pt', 'en'])) {
            Session::put('locale', $idioma);
        }
        return redirect()->back();
    })->name('lang');

    // Painel
    Route::group([
        'prefix'     => 'painel',
        'namespace'  => 'Painel',
        'middleware' => ['auth']
    ], function() {
        Route::get('/', 'PainelController@index')->name('painel');

        Route::group(['middleware' => 'admin'], function() {
            /* GENERATED ROUTES */
            Route::resource('home', 'HomeController', ['only' => ['index', 'update']]);
            Route::resource('certificacoes-provence', 'CertificacoesProvenceController', ['only' => ['index', 'update']]);
            Route::resource('certificacoes-provence-item', 'CertificacoesProvenceItemController');
            Route::resource('certificacoes-inmetro', 'CertificacoesInmetroController', ['only' => ['index', 'update']]);
            Route::resource('certificacoes-inmetro-item', 'CertificacoesInmetroItemController');
            Route::resource('certificacoes-de-sistemas', 'CertificacoesDeSistemasController', ['only' => ['index', 'update']]);
            Route::resource('perfil-empresa', 'PerfilEmpresaController', ['only' => ['index', 'update']]);
            Route::resource('nossos-servicos', 'NossosServicosController', ['only' => ['index', 'update']]);
            Route::resource('documentos-regulamentares', 'DocumentosRegulamentaresController');
            Route::resource('perguntas-frequentes', 'PerguntasFrequentesController');
            Route::resource('informacoes-e-novidades', 'InformacoesNovidadesController');
            Route::resource('fale-conosco', 'FaleConoscoController', ['only' => ['index', 'update']]);
            Route::resource('depoimentos', 'DepoimentosController');
            Route::resource('propostas', 'PropostasController');
            Route::resource('propostas-configuracoes', 'PropostasConfigController');
            Route::resource('validacao-certificados', 'ValidacaoController');
            Route::resource('certificados', 'CertificadosDeValidacaoController');
            Route::resource('banners', 'BannersController');
            Route::resource('certificado-de-sistemas-imagens', 'CertificadoSistemasImagensController');

            Route::resource('configuracoes', 'ConfiguracoesController', ['only' => ['index', 'update']]);

            Route::get('contato/recebidos/{recebidos}/toggle', ['as' => 'painel.contato.recebidos.toggle', 'uses' => 'ContatosRecebidosController@toggle']);
            Route::resource('contato', 'ContatoController');
            Route::get('contato/recebidos', 'ContatosRecebidosController@index')->name('painel.contato.recebidos');
            Route::resource('recebidos', 'RecebidosController');

            Route::resource('usuarios', 'UsuariosController');
            Route::resource('politica-de-privacidade', 'PoliticaDePrivacidadeController', ['only' => ['index', 'update']]);
            Route::get('aceite-de-cookies', 'AceiteDeCookiesController@index')->name('painel.aceite-de-cookies');

            Route::post('ckeditor-upload', 'PainelController@imageUpload');
            Route::post('order', 'PainelController@order');
            Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');

            Route::get('generator', 'GeneratorController@index')->name('generator.index');
            Route::post('generator', 'GeneratorController@submit')->name('generator.submit');
        });
    });

    // Auth
    Route::group([
        'prefix'    => 'painel',
        'namespace' => 'Auth'
    ], function() {
        Route::get('login', 'AuthController@showLoginForm')->name('auth');
        Route::post('login', 'AuthController@login')->name('login');
        Route::get('logout', 'AuthController@logout')->name('logout');
    });

    Route::get('/{slug}', 'ValidacaoController@index')->name('validacao-cliente');
});
