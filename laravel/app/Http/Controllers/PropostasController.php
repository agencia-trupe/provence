<?php

namespace App\Http\Controllers;

use App\Http\Requests;

use App\Models\Proposta;
use Illuminate\Http\Request;
use App\Http\Requests\PropostasRequest;
use App\Models\FaleConosco;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Mail;

class PropostasController extends Controller
{
    public function post(Request $request, Proposta $propostas)
    {   
        $validator = $this->validator($request);
        if ($validator->fails()) {
            return back()->withError('error', true)->withInput(); 
        }

        // $proposta = new Proposta();
        // $proposta->nome_empresa  = filter_var($request->nome_empresa, FILTER_SANITIZE_STRING);
        // $proposta->cnpj          = filter_var($request->cnpj, FILTER_SANITIZE_STRING);
        // $proposta->nome_contato  = filter_var($request->nome_contato, FILTER_SANITIZE_STRING);
        // $proposta->cargo         = filter_var($request->cargo, FILTER_SANITIZE_STRING);
        // $proposta->email         = filter_var($request->email, FILTER_VALIDATE_EMAIL);
        // $proposta->telefone      = filter_var($request->telefone, FILTER_SANITIZE_STRING);
        // $proposta->produto       = filter_var($request->produto, FILTER_SANITIZE_STRING);
        // $proposta->como_conheceu = filter_var($request->como_conheceu, FILTER_SANITIZE_STRING);
        // $proposta->situacao_cert = filter_var($request->situacao_cert, FILTER_SANITIZE_STRING);
        // $proposta->tipo_cert     = filter_var($request->tipo_cert, FILTER_SANITIZE_STRING);
        // $proposta->modelo_cert   = filter_var($request->modelo_cert, FILTER_SANITIZE_STRING);
        // $proposta->mensagem      = trim(filter_var($request->mensagem, FILTER_SANITIZE_STRING));

        // $proposta->save();
        
        $data = $request->all();

        $propostas->create($data);
        $this->sendMail2($data);

        return back()->with('enviado', true);

    }

    private function validator($fields)
    {
        $validator = Validator::make($fields->all(), [
            'nome_empresa' => 'required',
            'cnpj' => 'required',
            'nome_contato' => 'required',
            'cargo' => 'required',
            'email' => 'required|email',
            'telefone' => 'required',
            'produto' => '',
            'como_conheceu' => '',
            'situacao_cert' => '',
            'tipo_cert' => '',
            'modelo_cert' => '',
            'mensagem' => '',
        ]);

        return $validator;
    }

    public function sendMail2($data)
    {
        if (! $email = FaleConosco::first()->email_fale_conosco) {
            
           return false;
        }
        
        Mail::send('emails.proposta', $data, function($m) use ($email, $data)
        {
            $m->to($email, config('app.name'))
               ->subject('[CONTATO] '.config('app.name'))
               ->replyTo($data['email'], $data['nome_empresa']);
        });
    }
}
