<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Models\PropostasConfig;
use App\Models\PerguntasFrequentes;
use App\Models\InformacoesNovidades;
use App\Models\DocumentosRegulamentares;

class NovidadesController extends Controller
{
    public function index()
    {
        return view('frontend.suporte-e-novidades');
    }

    public function novidadesPaginaInterna($slug)
    {   
        $opcoes_form_proposta = PropostasConfig::all();
        $item = InformacoesNovidades::where('slug', $slug)->first();
        $items = InformacoesNovidades::limit(4)->get();
        return view('frontend.informacoes-e-novidades-pagina-interna', compact(
            'item', 'items', 'opcoes_form_proposta'
        ));
    }

    public function documentosRegulamentares()
    {
        $items = DocumentosRegulamentares::orderBy('ordem', 'asc')->get();
        return view('frontend.documentos-regulamentares', compact('items'));
    }

    public function perguntasFrequentes()
    {
        $items = PerguntasFrequentes::orderBy('ordem', 'asc')->get();
        return view('frontend.perguntas-frequentes', compact('items'));
    }

    public function novidades()
    {
        $items = InformacoesNovidades::orderBy('ordem', 'asc')->get();
        return view('frontend.informacoes-e-novidades', compact('items'));
    }
}
