<?php

namespace App\Http\Controllers;

use App\Models\Home;
use App\Models\Depoimento;
use Illuminate\Http\Request;
use App\Models\PerfilEmpresa;
use App\Models\NossosServicos;
use App\Models\AceiteDeCookies;
use App\Models\PropostasConfig;
use App\Models\InformacoesNovidades;
use App\Models\PoliticaDePrivacidade;
use App\Models\Banner;
use App\Models\Imagem;
use App\Models\CertificacoesInmetroItem;
use App\Models\CertificacoesProvenceItem;
use App\Models\CertificadoSistemasImagens;


class HomeController extends Controller
{
    public function index(Request $request)
    {
        $home = Home::first();
        $verificacao = AceiteDeCookies::where('ip', $request->ip())->first();
        $novidades = InformacoesNovidades::limit(3)->get();
        $depoimentos = Depoimento::all();
        $items = CertificacoesInmetroItem::orderBy('ordem', 'asc')->get();
        $items2 = CertificacoesProvenceItem::orderBy('ordem', 'asc')->get();
        $items3 = CertificadoSistemasImagens::orderBy('ordem', 'asc')->get();
        

        return view('frontend.home', compact(
            'home', 
            // 'banners',
            // 'imagens',
            'verificacao', 
            'novidades', 
            'depoimentos',
            'items',
            'items2',
            'items3',
        ));
    }

    public function clienteLogin()
    {
        return view('frontend.cliente');
    }

    public function postCookies(Request $request)
    {
        $input = $request->all();
        $input['ip'] = $request->ip();

        AceiteDeCookies::create($input);

        return redirect('/');
    }

    public function politicaDePrivacidade()
    {
        $registro = PoliticaDePrivacidade::first();
        return view('frontend.politica-de-privacidade', compact('registro'));
    }

    public function perfilEmpresa()
    {   
        $opcoes_form_proposta = PropostasConfig::all();
        $registro = PerfilEmpresa::first();
        return view('frontend.perfil-empresa', compact('registro', 'opcoes_form_proposta'));
    }

    public function nossosServicos()
    {   
        $opcoes_form_proposta = PropostasConfig::all();
        $registro = NossosServicos::first();
        return view('frontend.nossos-servicos', compact('registro', 'opcoes_form_proposta'));
    }    

}
