<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\FaleConoscoRequest;
use App\Http\Controllers\Controller;
use App\Models\FaleConosco;

class FaleConoscoController extends Controller
{
    public function index()
    {
        $registro = FaleConosco::first();
        return view('painel.fale-conosco.edit', compact('registro'));
    }

    public function update(FaleConoscoRequest $request, FaleConosco $registro)
    {
        try {
            $input = $request->all();
            $registro->first()->update($input);

            return redirect()->route('painel.fale-conosco.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}
