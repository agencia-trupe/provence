<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\ContatoRecebido;

class RecebidosController extends Controller
{
    public function index()
    {
        $contatos = ContatoRecebido::orderBy('created_at', 'DESC')->get();

        return view('painel.recebidos.index', compact('contatos'));
    }

    public function show(ContatoRecebido $recebido)
    {
        // $contato->update(['lido' => 1]);

        $recebido->update();

        return view('painel.recebidos.show', compact('recebido'));
    }

    public function destroy(ContatoRecebido $contato)
    {
        try {

            $contato->delete();
            return redirect()->route('painel.recebidos.index')->with('success', 'Mensagem excluída com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir mensagem: '.$e->getMessage()]);

        }
    }

    public function toggle(ContatoRecebido $contato, Request $request)
    {
        try {

            $contato->update([
                'lido' => !$contato->lido
            ]);

            return redirect()->route('painel.recebidos.index')->with('success', 'Mensagem alterada com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar mensagem: '.$e->getMessage()]);

        }
    }
}
