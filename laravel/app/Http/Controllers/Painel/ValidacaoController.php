<?php

namespace App\Http\Controllers\Painel;

use App\Http\Requests;
use App\Models\Validacao;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\ValidacaoRequest;
use App\Helpers\Tools;

class ValidacaoController extends Controller
{   
    public function index()
    {   
        $items = Validacao::all();

        return view('painel.validacao-certificados.index', compact('items'));
    }

    public function create()
    {
       $registro = "";
       return view('painel.validacao-certificados.create', compact('registro'));
    }

    public function store(ValidacaoRequest $request, Validacao $registro)
    {  
        try{
            $input = $request->all();

            if ($this->__existsSlug($input['slug'])) {
                throw new \Exception('Campo Slug já existe no sistema!');
            }

            if(!empty($input['imagem'])) $registro->imagem = Tools::fileUpload($input['imagem'],'validacao/');
            $registro->nome_cliente = $input['nome_cliente'];
            $registro->slug = $input['slug'];
            $registro->status = $input['status'] ?? false;
            $registro->save();

            return redirect()->route('painel.validacao-certificados.create')->with('success', 'Registro cadastrado com sucesso.');
        }catch(\Exception $e){
            return back()->withErrors(['Error ao cadastrar registro. ' . $e->getMessage()]);
        }
    }

    public function edit($id)
    {
        $registro = Validacao::find($id);
        if (! $registro) {
            return back()->withErrors(['Não encontrado essa validação']);
        }

        return view('painel.validacao-certificados.update', compact('registro'));
    }

    public function update(Request $request, $id)
    {
        try{
            $input = $request->all();
            $registro = Validacao::find($id);
            if (! $registro) {
                throw new \Exception('Não encontrado ID de validação.');
            }
            if ($registro->slug != $input['slug']) {
                if ($this->__existsSlug($input['slug'])) {
                    throw new \Exception('Campo Slug já existe no sistema.');
                }
                $registro->slug = $input['slug'];
            }
            if(!empty($input['imagem'])) {
                Tools::removeFileUpload($registro->imagem, 'validacao/');
                $registro->imagem = Tools::fileUpload($input['imagem'],'validacao/');
            } 

            $registro->nome_cliente = $input['nome_cliente'];
            $registro->status       = $input['status'] ?? false;
            $registro->save();

            return back()->with('success', 'Registro atualizado com sucesso.');
        }catch(\Exception $e){
            return back()->withErrors(['Error ao cadastrar registro. ' . $e->getMessage()]);
        }
    }

    public function destroy($id)
    {   
        try{    
            $registro = Validacao::find($id);
            if (! $registro) {
                throw new \Exception('Não encontrado ID do certificado.');
            }

            // Deletar todos certificados dessa Validação 
            $certificados = $registro->certificados()->get();
            if (!empty($certificados)) {
                foreach($certificados as $item) {
                    Tools::removeFileUpload($item->documento, 'certificados/');
                    $item->delete();
                }
            }

            // Deletar a Validação
            Tools::removeFileUpload($registro->imagem, 'validacao/');
            $registro->delete();
            
            return back()->with('success', 'Registro excluído com sucesso.');
        }catch(\Exception $e){
            return back()->withErrors(['Error ao deletar registro. ' . $e->getMessage()]);
        }
    }

    private function __existsSlug(string $slug)
    {
        $data = Validacao::select()->where('slug', $slug)->first();
        return $data ? true : false;
    }
}
