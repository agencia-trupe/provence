<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Models\CertificacoesProvenceItem;
use App\Http\Requests\CertificacoesItemRequest;
use App\Helpers\Tools;
use App\Models\CertificacoesInmetroItem;

class CertificacoesProvenceItemController extends Controller
{
    public function index(){
        $items = CertificacoesProvenceItem::orderBy('ordem', 'asc')->get();

        return view('painel.certificacoes-provence.items.index', compact('items'));
    }

    public function create()
    {
        $registro = "";
        return view('painel.certificacoes-provence.items.create', compact('registro'));
    }

    public function store(CertificacoesItemRequest $request, CertificacoesProvenceItem $registro)
    {
        try{
            $input = $request->all();

            // if(!empty($input['foto'])) $input['foto'] = Tools::fileUpload($input['foto'],'certificacoes-provence/');
            if(isset($input['foto'])) $input['foto'] = CertificacoesProvenceItem::uploadImagem1();
            if(!empty($input['documento'])) $input['documento'] = Tools::fileUpload($input['documento'],'certificacoes-provence/');

            $input['slug'] = str_slug($input['titulo_conteudo_pt']);
            $registro->create($input);

            return redirect()->route('painel.certificacoes-provence-item.index')->with('success', 'Registro cadastrado com sucesso.');
        }catch(\Exception $e){
            return back()->withErrors(['Error ao cadastrar registro. ' . $e->getMessage()]);
        }
    }

    public function edit($id){
        $registro = CertificacoesProvenceItem::find($id);
        return view('painel.certificacoes-provence.items.update', compact('registro'));
    }

    public function update(CertificacoesItemRequest $request, CertificacoesProvenceItem $registro, $id){
        try{
            $input = $request->all();
            
            if(isset($input['remove_documento'])){
                $input['foto'] = "";
                $input['documento'] = "";
            }else{
                if(isset($input['foto'])) $input['foto'] = CertificacoesProvenceItem::uploadImagem1();
                if(!empty($input['documento'])) $input['documento'] = Tools::fileUpload($input['documento'],'certificacoes-provence/');
            }

            $input['slug'] = str_slug($input['titulo_conteudo_pt']);
            unset($input['remove_documento']);

            $registro->find($id)->update($input);

            return redirect()->route('painel.certificacoes-provence-item.index')->with('success', 'Registro atualizado com sucesso.');
        }catch(\Exception $e){
            return back()->withErrors(['Error ao cadastrar registro. ' . $e->getMessage()]);
        }
    }

    public function destroy(CertificacoesProvenceItem $registro, $id){
        try{
            $registro->find($id)->delete();
            return back()->with('success', 'Registro excluído com sucesso.');
        }catch(\Exception $e){
            return back()->withErrors(['Error ao deletar registro. ' . $e->getMessage()]);
        }
    }
}
