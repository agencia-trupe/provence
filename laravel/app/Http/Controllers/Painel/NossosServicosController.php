<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\NossosServicosRequest;
use App\Http\Controllers\Controller;
use App\Models\NossosServicos;
use App\Helpers\Tools;

class NossosServicosController extends Controller
{
    public function index()
    {
        $registro = NossosServicos::first();
        return view('painel.nossos-servicos.edit', compact('registro'));
    }

    public function update(NossosServicosRequest $request)
    {
        try {
            $input = $request->all();
            $registro = NossosServicos::firstOrFail();

            // $registro->titulo     = $input['titulo'];
            // $registro->sub_titulo = $input['sub_titulo'];
            // $registro->descricao  = $input['descricao'];
            // $registro->titulo_2   = $input['titulo_2'];

            $registro->titulo_pt     = $input['titulo_pt'];
            $registro->sub_titulo_pt = $input['sub_titulo_pt'];
            $registro->descricao_pt  = $input['descricao_pt'];
            $registro->titulo_2_pt   = $input['titulo_2_pt'];

            $registro->titulo_en     = $input['titulo_en'];
            $registro->sub_titulo_en = $input['sub_titulo_en'];
            $registro->descricao_en  = $input['descricao_en'];
            $registro->titulo_2_en   = $input['titulo_2_en'];

            if(isset($input['remove_foto_1'])){
                Tools::removeFileUpload($registro->foto_1, 'nossos-servicos/');
                $registro->foto_1 = "";
            } else {
                if(isset($input['foto_1'])) {
                    Tools::removeFileUpload($registro->foto_1, 'nossos-servicos/');
                    $registro->foto_1 = Tools::fileUpload($input['foto_1'], 'nossos-servicos/');
                } 
            }

            if(isset($input['remove_foto_2'])) {
                Tools::removeFileUpload($registro->foto_2, 'nossos-servicos/');
                $registro->foto_2 = "";
            } else {
                if(isset($input['foto_2'])) {
                    Tools::removeFileUpload($registro->foto_2, 'nossos-servicos/');
                    $registro->foto_2 = Tools::fileUpload($input['foto_2'], 'nossos-servicos/');
                }
            }
            
            $registro->save();

            return redirect()->route('painel.nossos-servicos.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}
