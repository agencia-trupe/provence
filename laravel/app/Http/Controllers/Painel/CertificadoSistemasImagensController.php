<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\CertificadoSistemasImagensRequest;
use App\Http\Controllers\Controller;

use App\Models\CertificadoSistemasImagens;

class CertificadoSistemasImagensController extends Controller
{
    public function index()
    {
        $registros = CertificadoSistemasImagens::orderBy('ordem', 'asc')->get();

        return view('painel.certificado-de-sistemas-imagens.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.certificado-de-sistemas-imagens.create');
    }

    public function store(CertificadoSistemasImagensRequest $request)
    {
        try {

            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = CertificadoSistemasImagens::upload_imagem();

            CertificadoSistemasImagens::create($input);

            return redirect()->route('painel.certificado-de-sistemas-imagens.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(CertificadoSistemasImagens $registro)
    {
        return view('painel.certificado-de-sistemas-imagens.edit', compact('registro'));
    }

    public function update(CertificadoSistemasImagensRequest $request, CertificadoSistemasImagens $registro)
    {
        try {

            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = CertificadoSistemasImagens::upload_imagem();

            $registro->update($input);

            return redirect()->route('painel.certificado-de-sistemas-imagens.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(CertificadoSistemasImagens $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.certificado-de-sistemas-imagens.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
