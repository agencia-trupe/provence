<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\HomeRequest;
use App\Http\Controllers\Controller;
use App\Helpers\Tools;

use App\Models\Home;

class HomeController extends Controller
{
    public function index()
    {
        $registro = Home::first();

        return view('painel.home.edit', compact('registro'));
    }

    public function update(HomeRequest $request, Home $registro)
    {
        try {
            $input = $request->all();
                       
            if(!empty($input['banner_foto'])) $input['banner_foto'] = Tools::fileUpload($input['banner_foto'],'home/');
            // if(!empty($input['bloco_1_foto'])) $input['bloco_1_foto'] = Tools::fileUpload($input['bloco_1_foto'],'home/');
            // if(!empty($input['bloco_2_foto'])) $input['bloco_2_foto'] = Tools::fileUpload($input['bloco_2_foto'],'home/');
            // if(!empty($input['bloco_3_foto'])) $input['bloco_3_foto'] = Tools::fileUpload($input['bloco_3_foto'],'home/');

            if (isset($input['bloco_1_foto'])) $input['bloco_1_foto'] = Home::uploadImagem1();
            if (isset($input['bloco_2_foto'])) $input['bloco_2_foto'] = Home::uploadImagem2();
            if (isset($input['bloco_3_foto'])) $input['bloco_3_foto'] = Home::uploadImagem3();

            
            if(isset($input['carrossel'])){                
                $carrossel = $input['carrossel'];
                foreach($carrossel as $key => $item){
                    if(empty($item['foto'])){
                        $item['foto'] = $item['isFile'] ?? null;
                    }else{
                        $item['foto'] = Tools::fileUpload($item['foto'],'home/');
                    }
                    $carrossel[$key]['foto'] = $item['foto'];
                    unset($carrossel[$key]['isFile']);
                }
                $input['carrossel'] = json_encode($carrossel);
            }

            $registro->first()->update($input);

            return redirect()->route('painel.home.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }   

}
