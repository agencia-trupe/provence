<?php

namespace App\Http\Controllers\Painel;

use App\Models\Validacao;
use App\Models\CertificadosDeValidacao;

use App\Helpers\Tools;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\CertificadosDeValidacaoRequest;

class CertificadosDeValidacaoController extends Controller
{
    public function index(Request $request)
    {
        $registro = Validacao::find($request->id);
        if (! $registro) {
            return back()->withErrors(['Não encontrado essa validação']);
        }
        $items = $registro->certificados()->get();

        return view('painel.certificados.index', compact('items', 'registro'));
    }

    public function create(Request $request)
    {   
        $cliente = Validacao::find($request->id);
        if (! $cliente) {
            return back()->withErrors(['Não encontrado essa validação']);
        }

        $registro = "";
        return view('painel.certificados.create', compact('registro', 'cliente'));
    }

    public function store(CertificadosDeValidacaoRequest $request)
    {
        try {
            $registro = Validacao::find($request->id_validacao);
            if (! $registro) {
                throw new \Exception('Não encontrado essa validação');
            }

            $certificado = new CertificadosDeValidacao();
            $certificado->id_validacao = $request->id_validacao;
            $certificado->titulo = $request->titulo;
            $certificado->status = $request->status ?? false;
            $certificado->validade = $request->validade;

            if(!empty($request->documento)) { 
                $certificado->documento = Tools::fileUpload($request->documento,'certificados/');
            }

            $certificado->save();
            return back()->with('success', 'Registro atualizado com sucesso.');
        } catch(\Exception $e){
            return back()->withErrors(['Error ao cadastrar. ' . $e->getMessage()]);
        }
    }

    
    public function edit($id)
    {   
        $registro = CertificadosDeValidacao::find($id);
        if (! $registro) {
            return back()->withErrors(['Não encontrado esse certificado']);
        }
        $cliente = $registro->cliente()->first();

        return view('painel.certificados.update', compact('registro', 'cliente'));
    }

    public function update(CertificadosDeValidacaoRequest $request, $id)
    {
        try{
            $registro = CertificadosDeValidacao::find($id);
            if (! $registro) {
                throw new \Exception('Não encontrado ID do certificado.');
            }

            $cliente = $registro->cliente()->first();

            $registro->id_validacao = $cliente->id;
            $registro->titulo = $request->titulo;
            $registro->status = $request->status ?? false;
            $registro->validade = $request->validade;

            if(!empty($request->documento)) {
                Tools::removeFileUpload($request->documento,'certificados/');
                $registro->documento = Tools::fileUpload($request->documento,'certificados/');
            } 

            $registro->save();
            return back()->with('success', 'Registro atualizado com sucesso.');
        }catch(\Exception $e){
            return back()->withErrors(['Error ao cadastrar registro. ' . $e->getMessage()]);
        }
    }

    public function destroy($id)
    {
        try{    
            $registro = CertificadosDeValidacao::find($id);
            if (! $registro) {
                throw new \Exception('Não encontrado ID do certificado.');
            }

            Tools::removeFileUpload($registro->documento, 'certificados/');
            $registro->delete();
            
            return back()->with('success', 'Registro excluído com sucesso.');
        }catch(\Exception $e){
            return back()->withErrors(['Error ao deletar registro. ' . $e->getMessage()]);
        }
    }
}
