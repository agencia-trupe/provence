<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\InformacoesNovidades;
use App\Helpers\Tools;



class InformacoesNovidadesController extends Controller
{
    public function index()
    {
        $items = InformacoesNovidades::orderBy('ordem', 'asc')->get();
        return view('painel.informacoes-e-novidades.index', compact('items'));
    }

    public function create()
    {
        $registro = "";
        return view('painel.informacoes-e-novidades.create', compact('registro'));
    }

    public function store(Request $request, InformacoesNovidades $registro)
    {
        try{
            $input = $request->all();

            // if(isset($input['foto'])) $input['foto'] = Tools::fileUpload($input['foto'],'informacoes-e-novidades/');

            if (isset($input['foto'])) $input['foto'] = InformacoesNovidades::uploadFoto();
            
            $input['slug'] = str_slug($input['titulo_pt']);
            $registro->create($input);

            return redirect()->route('painel.informacoes-e-novidades.index')->with('success', 'Registro cadastrado com sucesso.');
        }catch(\Exception $e){
            return back()->withErrors(['Error ao cadastrar registro. ' . $e->getMessage()]);
        }
    }

    public function edit($id){
        $registro = InformacoesNovidades::find($id);
        return view('painel.informacoes-e-novidades.update', compact('registro'));
    }

    public function update(Request $request, InformacoesNovidades $registro, $id){
        try{
            $input = $request->all();
            
            // if(!empty($input['foto'])) $input['foto'] = Tools::fileUpload($input['foto'],'informacoes-e-novidades/');

            if (isset($input['foto'])) $input['foto'] = InformacoesNovidades::uploadFoto();
            
            $input['slug'] = str_slug($input['titulo_pt']);
            $registro->find($id)->update($input);

            return back()->with('success', 'Registro atualizado com sucesso.');
        }catch(\Exception $e){
            return back()->withErrors(['Error ao cadastrar registro. ' . $e->getMessage()]);
        }
    }

    public function destroy(InformacoesNovidades $registro, $id){
        try{
            $registro->find($id)->delete();
            return back()->with('success', 'Registro excluído com sucesso.');
        }catch(\Exception $e){
            return back()->withErrors(['Error ao deletar registro. ' . $e->getMessage()]);
        }
    }
}
