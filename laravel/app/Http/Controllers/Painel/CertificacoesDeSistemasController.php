<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;
use App\Http\Requests\CertificacoesRequest;
use App\Helpers\Tools;
use App\Http\Controllers\Controller;

use App\Models\CertificacoesDeSistemas;

class CertificacoesDeSistemasController extends Controller
{
    public function index()
    {
        $registro = CertificacoesDeSistemas::first();

        return view('painel.certificacoes-de-sistemas.edit', compact('registro'));
    }

    public function update(CertificacoesRequest $request)
    {
        try {
            $registro = CertificacoesDeSistemas::first();
            $input = $request->all();

            if(isset($input['remove_documento'])){
                Tools::removeFileUpload($registro->foto, 'certificacoes-de-sistemas/');
                $registro->foto = false;
            }
            if (!empty($input['foto'])) {
                Tools::removeFileUpload($registro->foto, 'certificacoes-de-sistemas/');
                $input['foto'] = Tools::fileUpload($input['foto'], 'certificacoes-de-sistemas/');
            }
            
            unset($input['remove_documento']);
            $registro->update($input);

            return redirect()->route('painel.certificacoes-de-sistemas.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}
