<?php

namespace App\Http\Controllers\Painel;

use App\Http\Requests;

use App\Models\Proposta;
use Illuminate\Http\Request;
use App\Models\PropostasConfig;
use App\Http\Controllers\Controller;
use App\Http\Requests\PropostasRequest;

class PropostasController extends Controller
{
    public function index()
    {   
        $registro = Proposta::all();
        
        return view('painel.propostas.index', compact('registro'));
    }

    public function create()
    {
        $registro = "";
        $lista_como_conheceu     = PropostasConfig::select()->where('categoria', 'como_conheceu')->get();  
        $lista_situacao_cert     = PropostasConfig::select()->where('categoria', 'situacao_cert')->get();
        $lista_tipo_cert         = PropostasConfig::select()->where('categoria', 'tipo_cert')->get();
        $lista_modelo_cert       = PropostasConfig::select()->where('categoria', 'modelo_cert')->get();

        return view('painel.propostas.create', compact(
            'registro', 'lista_como_conheceu', 'lista_situacao_cert',
            'lista_tipo_cert', 'lista_modelo_cert'
        ));
    }

    public function store(PropostasRequest $request, Proposta $registro)
    {
        try{
            $input = $request->all();
            $registro->create($input);

            return redirect()->route('painel.propostas.create')->with('success', 'Registro cadastrado com sucesso.');
        }catch(\Exception $e){
            return back()->withErrors(['Error ao cadastrar registro. ' . $e->getMessage()]);
        }
    }

    public function edit($id){
        $registro = Proposta::find($id);
        $lista_como_conheceu = PropostasConfig::select()->where('categoria', 'como_conheceu')->get();  
        $lista_situacao_cert = PropostasConfig::select()->where('categoria', 'situacao_cert')->get();
        $lista_tipo_cert     = PropostasConfig::select()->where('categoria', 'tipo_cert')->get();
        $lista_modelo_cert   = PropostasConfig::select()->where('categoria', 'modelo_cert')->get();

        return view('painel.propostas.update', compact(
            'registro', 'lista_como_conheceu', 'lista_situacao_cert',
            'lista_tipo_cert', 'lista_modelo_cert'
        ));
    }

    public function update(PropostasRequest $request, Proposta $registro, $id){
        try{
            $input = $request->all();
            $registro->find($id)->update($input);

            return back()->with('success', 'Registro atualizado com sucesso.');
        }catch(\Exception $e){
            return back()->withErrors(['Error ao cadastrar registro. ' . $e->getMessage()]);
        }
    }

    public function destroy(Proposta $registro, $id){
        try{
            $registro->find($id)->delete();
            return back()->with('success', 'Registro excluído com sucesso.');
        }catch(\Exception $e){
            return back()->withErrors(['Error ao deletar registro. ' . $e->getMessage()]);
        }
    }
}
