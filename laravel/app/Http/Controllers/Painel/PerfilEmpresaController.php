<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\CertificacoesRequest;
use App\Http\Controllers\Controller;
use App\Models\PerfilEmpresa;

class PerfilEmpresaController extends Controller
{
    public function index()
    {
        $registro = PerfilEmpresa::first();
        return view('painel.perfil-empresa.edit', compact('registro'));
    }

    public function update(CertificacoesRequest $request, PerfilEmpresa $registro)
    {
        try {
            $input = $request->all();
            $registro->first()->update($input);

            return redirect()->route('painel.perfil-empresa.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}
