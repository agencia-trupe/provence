<?php

namespace App\Http\Controllers\Painel;

use App\Helpers\Tools;

use App\Http\Requests;
use App\Models\Depoimento;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DepoimentosController extends Controller
{
    public function index()
    {
        $items = Depoimento::all();
        return view('painel.depoimentos.index', compact('items'));
    }

    public function create()
    {
        $registro = "";
        return view('painel.depoimentos.create', compact('registro'));
    }

    public function store(Request $request, Depoimento $registro)
    {
        try{
            $input = $request->only(['descricao_pt', 'descricao_en', 'foto']);

            if(isset($input['foto'])) $input['foto'] = Tools::fileUpload($input['foto'],'depoimentos/');

            $registro->descricao_pt = $input['descricao_pt'];
            $registro->descricao_en = $input['descricao_en'];
            $registro->foto      = $input['foto'];
            $registro->save();

            return redirect()->route('painel.depoimentos.create')->with('success', 'Registro cadastrado com sucesso.');
        }catch(\Exception $e){
            return back()->withErrors(['Error ao cadastrar registro. ' . $e->getMessage()]);
        }
    }

    public function edit($id)
    {
        $registro = Depoimento::find($id);
        return view('painel.depoimentos.update', compact('registro'));
    }

    public function update(Request $request, $id){
        try{
            $depoimento = Depoimento::where('id', $id)->first();
            $input = $request->only(['descricao_pt', 'descricao_en', 'foto']);
            
            if (!empty($input['foto'])) {
                $depoimento->foto = Tools::fileUpload($input['foto'],'depoimentos/');
            }
            $depoimento->descricao_pt = $input['descricao_pt'];
            $depoimento->descricao_en = $input['descricao_en'];
            $depoimento->save();

            return back()->with('success', 'Registro atualizado com sucesso.');
        }catch(\Exception $e){
            return back()->withErrors(['Error ao cadastrar registro. ' . $e->getMessage()]);
        }
    }

    public function destroy(Depoimento $registro, $id){
        try{
            $registro->find($id)->delete();
            return back()->with('success', 'Registro excluído com sucesso.');
        }catch(\Exception $e){
            return back()->withErrors(['Error ao deletar registro. ' . $e->getMessage()]);
        }
    }
}
