<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\CertificacoesRequest;
use App\Http\Controllers\Controller;
use App\Helpers\Tools;
use App\Models\CertificacoesInmetro;

class CertificacoesInmetroController extends Controller
{
    public function index()
    {
        $registro = CertificacoesInmetro::first();

        return view('painel.certificacoes-inmetro.edit', compact('registro'));
    }

    public function update(CertificacoesRequest $request)
    {
        try {
            $input = $request->all();
            $registro = CertificacoesInmetro::first();
            $registro->update($input);

            return redirect()->route('painel.certificacoes-inmetro.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}
