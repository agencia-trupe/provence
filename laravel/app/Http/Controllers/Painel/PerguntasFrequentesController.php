<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\PerguntasFrequentes;

class PerguntasFrequentesController extends Controller
{
    public function index()
    {
        $items = PerguntasFrequentes::orderBy('ordem', 'asc')->get();
        return view('painel.perguntas-frequentes.index', compact('items'));
    }

    public function create()
    {
        $registro = "";
        return view('painel.perguntas-frequentes.create', compact('registro'));
    }

    public function store(Request $request, PerguntasFrequentes $registro)
    {
        try{
            $input = $request->all();
            $registro->create($input);

            return redirect()->route('painel.perguntas-frequentes.create')->with('success', 'Registro cadastrado com sucesso.');
        }catch(\Exception $e){
            return back()->withErrors(['Error ao cadastrar registro. ' . $e->getMessage()]);
        }
    }

    public function edit($id){
        $registro = PerguntasFrequentes::find($id);
        return view('painel.perguntas-frequentes.update', compact('registro'));
    }

    public function update(Request $request, PerguntasFrequentes $registro, $id){
        try{
            $input = $request->all();
            $registro->find($id)->update($input);

            return back()->with('success', 'Registro atualizado com sucesso.');
        }catch(\Exception $e){
            return back()->withErrors(['Error ao cadastrar registro. ' . $e->getMessage()]);
        }
    }

    public function destroy(PerguntasFrequentes $registro, $id){
        try{
            $registro->find($id)->delete();
            return back()->with('success', 'Registro excluído com sucesso.');
        }catch(\Exception $e){
            return back()->withErrors(['Error ao deletar registro. ' . $e->getMessage()]);
        }
    }
}
