<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\CertificacoesRequest;
use App\Models\CertificacoesProvence;
use App\Helpers\Tools;

class CertificacoesProvenceController extends Controller
{
    public function index()
    {
        $registro = CertificacoesProvence::first();

        return view('painel.certificacoes-provence.edit', compact('registro'));
    }

    public function update(CertificacoesRequest $request, CertificacoesProvence $registro)
    {
        try {
            $input = $request->all();

            $registro->first()->update($input);

            return redirect()->route('painel.certificacoes-provence.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}
