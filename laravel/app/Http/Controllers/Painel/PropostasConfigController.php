<?php

namespace App\Http\Controllers\Painel;

use App\Http\Requests;

use Illuminate\Http\Request;
use App\Models\PropostasConfig;
use App\Http\Controllers\Controller;
use App\Http\Requests\PropostasConfigRequest;

class PropostasConfigController extends Controller
{   
    public function index()
    {   
        $registro = PropostasConfig::all();
        return view('painel.propostas-config.index', compact('registro'));
    }

    public function create()
    {
        $registro = "";
        return view('painel.propostas-config.create', compact('registro'));
    }

    public function store(PropostasConfigRequest $request, PropostasConfig $registro)
    {
        try{
            $input = $request->all();
            $registro->create($input);

            return redirect()->route('painel.propostas-configuracoes.create')->with('success', 'Registro cadastrado com sucesso.');
        }catch(\Exception $e){
            return back()->withErrors(['Error ao cadastrar registro. ' . $e->getMessage()]);
        }
    }

    public function edit($id){
        $registro = PropostasConfig::find($id);
        return view('painel.propostas-config.update', compact('registro'));
    }

    public function update(PropostasConfigRequest $request, PropostasConfig $registro, $id){
        try{
            $input = $request->all();
            $registro->find($id)->update($input);

            return back()->with('success', 'Registro atualizado com sucesso.');
        }catch(\Exception $e){
            return back()->withErrors(['Error ao cadastrar registro. ' . $e->getMessage()]);
        }
    }

    public function destroy(PropostasConfig $registro, $id){
        try{
            $registro->find($id)->delete();
            return back()->with('success', 'Registro excluído com sucesso.');
        }catch(\Exception $e){
            return back()->withErrors(['Error ao deletar registro. ' . $e->getMessage()]);
        }
    }
}
