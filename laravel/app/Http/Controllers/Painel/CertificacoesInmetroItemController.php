<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Http\Requests\CertificacoesItemRequest;
use App\Models\CertificacoesInmetro;
use App\Models\CertificacoesInmetroItem;
use App\Helpers\Tools;

class CertificacoesInmetroItemController extends Controller
{

    public function index(){

        
        // $items = CertificacoesInmetroItem::ordenados()->get();
        $items = CertificacoesInmetroItem::orderBy('ordem', 'asc')->get();

        return view('painel.certificacoes-inmetro.items.index', compact('items'));
    }

    public function create()
    {
        $registro = "";
        return view('painel.certificacoes-inmetro.items.create', compact('registro'));
    }

    public function store(CertificacoesItemRequest $request, CertificacoesInmetroItem $registro)
    {
        try{
            $input = $request->all();

            // if(isset($input['foto'])) $input['foto'] = Tools::fileUpload($input['foto'],'certificacoes-inmetro/');
            if(isset($input['foto'])) $input['foto'] = CertificacoesInmetroItem::uploadImagem1();
            if(!empty($input['documento'])) $input['documento'] = Tools::fileUpload($input['documento'],'certificacoes-inmetro/');

            $input['slug'] = str_slug($input['titulo_conteudo_pt']);
            $registro->create($input);

            return redirect()->route('painel.certificacoes-inmetro-item.index')->with('success', 'Registro cadastrado com sucesso.');
        }catch(\Exception $e){
            return back()->withErrors(['Error ao cadastrar registro. ' . $e->getMessage()]);
        }
    }

    public function edit($id){
        $registro = CertificacoesInmetroItem::find($id);
        return view('painel.certificacoes-inmetro.items.update', compact('registro'));
    }

    public function update(CertificacoesItemRequest $request, CertificacoesInmetroItem $registro, $id){
        try{
            $input = $request->all();
            
            if(isset($input['remove_documento'])){
                $input['foto'] = "";
                $input['documento'] = "";
            }else{
                // if(isset($input['foto'])) $input['foto'] = Tools::fileUpload($input['foto'],'certificacoes-inmetro/');
                if(isset($input['foto'])) $input['foto'] = CertificacoesInmetroItem::uploadImagem1();
                if(!empty($input['documento'])) $input['documento'] = Tools::fileUpload($input['documento'],'certificacoes-inmetro/');
            }

            $input['slug'] = str_slug($input['titulo_conteudo_pt']);
            unset($input['remove_documento']);

            $registro->find($id)->update($input);

            return redirect()->route('painel.certificacoes-inmetro-item.index')->with('success', 'Registro atualizado com sucesso.');
        }catch(\Exception $e){
            return back()->withErrors(['Error ao cadastrar registro. ' . $e->getMessage()]);
        }
    }

    public function destroy(CertificacoesInmetroItem $registro, $id){
        try{
            $registro->find($id)->delete();
            return back()->with('success', 'Registro excluído com sucesso.');
        }catch(\Exception $e){
            return back()->withErrors(['Error ao deletar registro. ' . $e->getMessage()]);
        }
    }
}
