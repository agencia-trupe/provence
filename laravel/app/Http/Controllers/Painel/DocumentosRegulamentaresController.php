<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\DocumentosRegulamentares;
use App\Helpers\Tools;
use App\Http\Requests\DocumentosRegulamentaresRequest;

class DocumentosRegulamentaresController extends Controller
{
    public function index()
    {
        $items = DocumentosRegulamentares::orderBy('ordem', 'asc')->get();

        return view('painel.documentos-regulamentares.index', compact('items'));
    }

    public function create()
    {
        $registro = "";
        return view('painel.documentos-regulamentares.create', compact('registro'));
    }

    public function store(DocumentosRegulamentaresRequest $request, DocumentosRegulamentares $registro)
    {
        try{
            $input = $request->all();

            if(isset($input['documento'])) $input['documento'] = Tools::fileUpload($input['documento'],'documentos-regulamentares/');
            
            $registro->create($input);

            return redirect()->route('painel.documentos-regulamentares.create')->with('success', 'Registro cadastrado com sucesso.');
        }catch(\Exception $e){
            return back()->withErrors(['Error ao cadastrar registro. ' . $e->getMessage()]);
        }
    }

    public function edit($id){
        $registro = DocumentosRegulamentares::find($id);
        return view('painel.documentos-regulamentares.update', compact('registro'));
    }

    public function update(DocumentosRegulamentaresRequest $request, DocumentosRegulamentares $registro, $id){
        try{
            $input = $request->all();
            
            if(!empty($input['documento'])) $input['documento'] = Tools::fileUpload($input['documento'],'documentos-regulamentares/');

            $registro->find($id)->update($input);

            return back()->with('success', 'Registro atualizado com sucesso.');
        }catch(\Exception $e){
            return back()->withErrors(['Error ao cadastrar registro. ' . $e->getMessage()]);
        }
    }

    public function destroy(DocumentosRegulamentares $registro, $id){
        try{
            $registro->find($id)->delete();
            return back()->with('success', 'Registro excluído com sucesso.');
        }catch(\Exception $e){
            return back()->withErrors(['Error ao deletar registro. ' . $e->getMessage()]);
        }
    }
}
