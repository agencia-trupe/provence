<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ContatoRequest;
use App\Models\FaleConosco;
use App\Models\ContatoRecebido;
use Illuminate\Support\Facades\Mail;

class FaleConoscoController extends Controller
{
    public function index()
    {
        $registro = FaleConosco::first();

        return view('frontend.fale-conosco', compact('registro'));
    }

    public function post(ContatoRequest $request, ContatoRecebido $contatoRecebido)
    {
        $data = $request->all();

        $contatoRecebido->create($data);
        $this->sendMail($data);

        return back()->with('enviado', true);
    }

    private function sendMail($data)
    {
        if (! $email = FaleConosco::first()->email_fale_conosco) {
           return false;
        }

        Mail::send('emails.contato', $data, function($m) use ($email, $data)
        {
            $m->to($email, config('app.name'))
               ->subject('[CONTATO] '.config('app.name'))
               ->replyTo($data['email'], $data['nome']);
        });
    }
}
