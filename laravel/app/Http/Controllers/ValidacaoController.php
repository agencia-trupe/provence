<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\Validacao;
use App\Models\CertificadosDeValidacao;

class ValidacaoController extends Controller
{
    public function index($slug)
    {
        $cliente = Validacao::select()->where('slug', $slug)->first();
        // Verificar se Slug do cliente não existe ou se desativado. 
        if (! $cliente || $cliente->status == false) {
            return redirect()->route('home');
        }

        // Pegar certificados que estão válido e status ativado
        $dataAtual = date('Y-m-d');
        $cliente->certificados = $cliente->certificados()
            ->where('status', true)
            ->where('validade', '>', $dataAtual)
        ->get();

        $headerFalse = true;
        return view('frontend.validacao-cliente', compact('headerFalse', 'cliente'));
    }
}
