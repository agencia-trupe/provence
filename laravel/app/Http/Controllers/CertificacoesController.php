<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\PropostasConfig;
use App\Models\CertificacoesInmetro;
use App\Models\CertificacoesProvence;
use App\Models\CertificacoesDeSistemas;
use App\Models\CertificacoesInmetroItem;
use App\Models\CertificacoesProvenceItem;

class CertificacoesController extends Controller
{
    public function provence()
    {   
        $opcoes_form_proposta = PropostasConfig::all();
        $registro = CertificacoesProvence::first();
        $registro_items = CertificacoesProvenceItem::orderBy('ordem', 'asc')->get();
        return view('frontend.certificacoes-provence', compact(
            'registro', 'registro_items', 'opcoes_form_proposta'
        ));
    }

    public function provencePaginaInterna($params)
    {   
        $registro = CertificacoesProvenceItem::where('slug', $params)->first();
        if (! $registro) {
            return redirect()->route('home');
        }

        $registro_principal = CertificacoesInmetro::first();
        $opcoes_form_proposta = PropostasConfig::all();
        $registro_items = CertificacoesProvenceItem::all();
        return view('frontend.certificacoes-provence-pagina-interna',
            compact(
            'registro_principal',
            'registro',
            'registro_items', 
            'opcoes_form_proposta'
        ));
    }

    public function inmetro()
    {
        $registro = CertificacoesInmetro::first();
        $registro_items = CertificacoesInmetroItem::orderBy('ordem', 'asc')->get();
        $opcoes_form_proposta = PropostasConfig::all();
        return view('frontend.certificacoes-inmetro', compact(
            'registro', 'registro_items', 'opcoes_form_proposta'
        ));
    }

    public function inmetroPaginaInterna($params)
    {   
        $registro = CertificacoesInmetroItem::where('slug', $params)->first();
        if (! $registro) {
            return redirect()->route('home');
        }

        $registro_principal = CertificacoesInmetro::first();
        $opcoes_form_proposta = PropostasConfig::all();
        $registro_items = CertificacoesInmetroItem::all();
        return view('frontend.certificacoes-inmetro-pagina-interna',
        compact(
            'registro_principal',
            'registro',
            'registro_items', 
            'opcoes_form_proposta'
        ));
    }

    public function deSistemas()
    {   
        $registro = CertificacoesDeSistemas::first();
        $opcoes_form_proposta = PropostasConfig::all();
        return view('frontend.certificacoes-de-sistemas', compact(
            'registro' , 'opcoes_form_proposta'
        ));
    }

    public function deSistemasPaginaInterna($params)
    {
        $registro = CertificacoesDeSistemasItem::where('slug', $params)->first();
        if (! $registro) {
            return redirect()->route('home');
        }

        $opcoes_form_proposta = PropostasConfig::all();
        $registro_items = CertificacoesDeSistemasItem::all();
        return view('frontend.certificacoes-de-sistemas-pagina-interna', compact(
            'registro','registro_items', 'opcoes_form_proposta'
        ));
    }
}
