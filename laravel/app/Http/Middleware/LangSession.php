<?php

namespace App\Http\Middleware;

use Closure;

class LangSession
{
    public function handle($request, Closure $next)
    {
    	$locale = $request->session()->has('locale') ? $request->session()->get('locale') : 'pt';
        app()->setLocale($locale);

        return $next($request);
    }
}
